function strPatients=getPatientsInfor_b(xmlPath)
    % This script is used just in case of not having a subjects directory.
    % In this case a list with the xml files is generated.
    
    listFichXML = dir(strcat(xmlPath,'*.xml'));
    numImgs = numel(listFichXML);
    
    
    strPatients(1,numImgs)=struct('fsid',[],'years',[],'Age',[],'sex',[],...
        'GDS',[],'diagnose','EXAMDATE',[],'imageUID',[]);
    
    % In the main loop, unlike the common version, it is not necessary to
    % check for coincidence between xml file and the subject's visit as
    % there is no subject. The xml information is simply asigned to the
    % struct.
    
    for i=1:numImgs
        
        filename = strcat(xmlpath,listFichXML(i).name);

        [strPatients(i).sex,strPatients(i).Age,...
            strPatients(i).GDS,strPatients(i).diagnose,...
            strPatients(i).visit,strPatients(i).EXAMDATE,strPatients(i).imageUID]=...
            Read_xml2structPPMI(filename);
        
        strPatients(i).fsid=strcact(listFichXML(i).name(1:7));
        
        fprintf('%s %s %.3f %s %.1f \n',...
            listFichXML(i).name,strPatients(i).sex,...
            strPatients(i).Age,strPatients(i).diagnose,...
            strPatients(i).GDS);
    end
end