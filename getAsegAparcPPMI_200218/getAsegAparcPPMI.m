function getAsegAparcPPMI
    % Generates files with clinical and demographic data * .dat, as well as
    % information of subcortical structures (aseg) and cortical (aparc)
    % files. This files will later be converted into tables and more
    % neuropsicological factors will be added in form of new columns.
    % It is prepared for the long. For the cross comment the long code and
    % uncomment the cross.
    
    %% Freesurfer variables

     
    %% data
    xmlpath= '../xml/';
    path_subj='../Long/';

    %% Long
    % For this type of processing SUBJECTS_DIR must be changed to the Long
    % folder
    
    typePross='Long';
    name_dat='clinicaldata_long_68V_23s.dat';

    
    %QdecTable(xmlpath,path_subj,typePross,name_dat);
    
    % generation of aseg file with subcortical structures info. The
    % --common-segs flag makes data homogenous ensuring that only the
    % segmentations obtained in all visits are used to build the aseg file.
    command= strcat('asegstats2table --qdec-long',32,name_dat,32,...
        '-t ./aseg_long.',name_dat,32,'--common-segs --skip');
    disp(command);
    system(command);
    
    % generation of aparc file with left hemisphere cortical structures
    % info.
    command= strcat('aparcstats2table --qdec-long',32,name_dat,32,...
        '-t ./aparc_long_lh.',name_dat,32,'--hemi lh --meas thickness --skip');
    disp(command);
    system(command);
    
    % generation of aparc file with right hemisphere cortical structures
    % info.
    command= strcat('aparcstats2table --qdec-long',32,name_dat,32,...
        '-t ./aparc_long_rh.',name_dat,32,'--hemi rh --meas thickness --skip');
    disp(command);
    system(command);
    
end