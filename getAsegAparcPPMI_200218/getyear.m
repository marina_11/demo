function [sX] = getyear(sX,ni)
    % This function fulfills the 'year' column taking age and the number of
    % images (ni) for each subject.
    suj=0;
    for i=1:numel(ni)
        for v=1:ni(i)
            if v==1
                if sX{suj+v,7} == 'Baseline'
                    basetime=sX{suj+v,4};
                elseif sX{suj+v,7} == 'Month_12'
                        basetime = 1;
                elseif sX{suj+v,7} == 'Month_24'
                    basetime = 2;
                elseif sX{suj+v,7} == 'Month_48'
                    basetime = 4;
                end
            end
            sX{suj+v,2}=sX {suj+v,4}-basetime;
        end
        suj=suj+ni(i);
    end
    
end