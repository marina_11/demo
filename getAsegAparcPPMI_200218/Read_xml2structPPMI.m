function [sex,Age,GDS,diagnose,ID,visit,imageUID,EXAMDATE]=Read_xml2structPPMI(filename)

%strPatients(1,numImgs)=struct('sex',[],'Age',[],'GDS',[],'diagnose',[],'ID',[],'visit',[],'imageUID',[],'EXAMDATE',[]);

%% Reading the xml file
%for i=1:length(listFichXML)
%filename = strcat(xmlpath,listFichXML(i).name);
theStruct = xml2struct(filename);

% These parts of the struct are repeatedly used over the function.
data1 = theStruct.idaxs.project.subject;

%% Guaranteed parameters
% These parameters are always expected to be found, and so they're not
% checked.

%% sex, id, diagnose, age, date, uid
sex = data1.subjectSex.Text;
ID = str2double(data1.subjectIdentifier.Text);

visit = data1.visit.visitIdentifier.Text;
if visit == 'Month 12'
    visit = 'Month_12';
elseif visit == 'Month 24'
    visit = 'Month_24';
elseif visit == 'Month 36'
    visit = 'Month_36';
elseif visit == 'Month 48'
    visit = 'Month_48';
elseif visit == 'Month 60'
    visit = 'Month_60';
else 
    visit = visit;
end
    
diagnose= data1.researchGroup.Text;
Age=str2double(data1.study.subjectAge.Text);
EXAMDATE=data1.study.series.dateAcquired.Text;
q = isfield(data1.study, 'imagingProtocol');
if q==1
    imageUID=str2double(data1.study.imagingProtocol.imageUID.Text);
else 
     imageUID=str2double(data1.study.series.seriesLevelMeta.derivedProduct.imageUID.Text);
end
%% GDS 
GDS=-1;
a = isfield(data1.visit,'assessment');
if a == 1
GDS=str2double(data1.visit.assessment.component.assessmentScore.Text);
 %       strPatients(i).GDS = GDS;
end

% strPatients(i).sex = sex;
% strPatients(i).ID = ID;
% strPatients(i).visit = visit;
% strPatients(i).diagnose = diagnose;
% strPatients(i).Age = Age;
% strPatients(i).EXAMDATE = EXAMDATE;
% strPatients(i).imageUID = imageUID;

%end

% XMLinfo = struct2table(strPatients);

end