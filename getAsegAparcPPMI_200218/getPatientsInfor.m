function strPatients=getPatientsInfor(path_subj,xmlpath)
    %This is the default function to take info from xml files.   
    % First all subjects directories are stored in listImgs and then . and
    % .. are removed from the list.
    listImgs = dir(strcat(path_subj));
    listImgs = listImgs(3:end);%.,..,fsaverage    
    
    % A list of all xml files is generated
    listFichXML = dir(strcat(xmlpath));
    listFichXML=listFichXML(3:end);
    numImgs = numel(listImgs);
    
    strPatients(1,numImgs)=struct('fsid',[],'years',[],'sex',[],'Age',[],'GDS',[],'diagnose',[],...
        'visit',[],'imageUID',[],'EXAMDATE',[]);

    
    % In the main loop a match between the xml file and the visit name is
    % searched in order to asign the values of the XML to the strPatients
    % struct.
    for i=1:numImgs
       for j=1:numel(listFichXML)
%             if(strcmp(listImgs(i).name,listFichXML(j).name(1:end-4)))
             if(strcmp(listImgs(i).name(1:end-10),listFichXML(j).name(1:end-4)))
                
                filename = strcat(xmlpath,'/',listFichXML(j).name);

               [strPatients(i).sex,strPatients(i).Age,strPatients(i).GDS,...
                    strPatients(i).diagnose,strPatients(i).ID,strPatients(i).visit,...
                    strPatients(i).imageUID,strPatients(i).EXAMDATE]=...
                    Read_xml2structPPMI(filename);

                
                strPatients(i).fsid=listFichXML(j).name(1:end-4);
                
                fprintf('%s %s %.3f %s %.1f\n',...
                    listFichXML(j).name(1:end-4),strPatients(i).sex,...
                    strPatients(i).Age,strPatients(i).diagnose,...
                    strPatients(i).GDS);
            else
                fprintf('Nii: %s, Xml: %s are differents\n',...
                    listImgs(i).name,listFichXML(j).name(1:end-4));
            end
       end
        
    end
    
end