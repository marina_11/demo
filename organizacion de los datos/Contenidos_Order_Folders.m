%% Order_Folders
% Se presentan los Scripts utilizados en la organización de las MRI y sus
% respectivos archivos XML descargados de PPMI 

%1. Order_xml: Lee la información del archivo xml y en función de la visita
%cambia el nombre del archivo (ID_visita.xml)

%2. Order_Images: Renombra las imágenes con el identificador del sujeto seguido
% de su visita (BL, V04, V06 y V10) quedando de la forma ID_visita.nii y las agupa 
% a todas en un mismo directorio.