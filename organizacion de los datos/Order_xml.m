function Order_xml
xmlpath = 'C:/Users/marin/Desktop/TFG/ordenar/Data/xml/';

xmldestiny = 'C:/Users/marin/Desktop/TFG/ordenar/Data/xml_ordered/';

listFichXML = dir(strcat(xmlpath,'*.xml'));
numXML = numel(listFichXML);

strPatients(1,numXML)=struct('sex',[],'Age',[],'GDS',[],'diagnose',[],'ID',[],'visit',[],'imageUID',[],'EXAMDATE',[]);

%% Reading the xml file
for i=1:length(listFichXML)
    filename = strcat(xmlpath,listFichXML(i).name);
theStruct = xml2struct(filename);

% These parts of the struct are repeatedly used over the function.
data1 = theStruct.idaxs.project.subject;

%% Guaranteed parameters
% These parameters are always expected to be found, and so they're not
% checked.

%% sex, id, diagnose, age, date, uid
sex = data1.subjectSex.Text;
ID = str2double(data1.subjectIdentifier.Text);
visit = data1.visit.visitIdentifier.Text;
diagnose= data1.researchGroup.Text;
Age=str2double(data1.study.subjectAge.Text);
EXAMDATE=data1.study.series.dateAcquired.Text;
%imageUID=str2double(data1.study.imagingProtocol.imageUID.Text);

%% GDS 
GDS=-1;
a = isfield(data1.visit,'assessment');
if a == 1
GDS=str2double(data1.visit.assessment.component.assessmentScore.Text);
        strPatients(i).GDS = GDS;
end

strPatients(i).sex = sex;
strPatients(i).ID = ID;
strPatients(i).visit = visit;
strPatients(i).diagnose = diagnose;
strPatients(i).Age = Age;
strPatients(i).EXAMDATE = EXAMDATE;
%strPatients(i).imageUID = imageUID;
 
   
if strPatients(i).visit == 'Baseline'
    FileIn=strcat(xmlpath,listFichXML(i).name); %Ruta de la imagen
    FileIn=fullfile(FileIn);
    FileOut=strcat(xmldestiny,listFichXML(i).name(6:9),'_BL.xml'); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
    FileOut=fullfile(FileOut);
    command = ['copy ' FileIn ' ' FileOut];
    system(command);
    
elseif strPatients(i).visit == 'Month 12'
    FileIn=strcat(xmlpath,listFichXML(i).name); %Ruta de la imagen
    FileIn=fullfile(FileIn);
    FileOut=strcat(xmldestiny,listFichXML(i).name(6:9),'_V04.xml'); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
    FileOut=fullfile(FileOut);
    command = ['copy ' FileIn ' ' FileOut];
    system(command);
    
elseif strPatients(i).visit == 'Month 24'
    FileIn=strcat(xmlpath,listFichXML(i).name); %Ruta de la imagen
    FileIn=fullfile(FileIn);
    FileOut=strcat(xmldestiny,listFichXML(i).name(6:9),'_V06.xml'); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
    FileOut=fullfile(FileOut);
    command = ['copy ' FileIn ' ' FileOut];
    system(command);
    
elseif strPatients(i).visit == 'Month 48' 
    FileIn=strcat(xmlpath,listFichXML(i).name); %Ruta de la imagen
    FileIn=fullfile(FileIn);
    FileOut=strcat(xmldestiny,listFichXML(i).name(6:9),'_V10.xml'); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
    FileOut=fullfile(FileOut);
    command = ['copy ' FileIn ' ' FileOut];
    system(command);
end

end
end
