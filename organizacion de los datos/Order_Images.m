function Order_Images
clear; clc;

source_dir = 'C:/Users/marin/Desktop/TFG/ordenar/Data/Images'; %Ruta de las imagenes de los sujetos PD-NC
folders = dir(source_dir);


pathId_all = 'C:/Users/marin/Desktop/TFG/ordenar/Data/nii'; %Ruta en la que se guardaran todas las imagenes ordenadas


pathXml_ordered='C:/Users/marin/Desktop/TFG/ordenar/Data/xml_ordered/';

listFichXML = dir(strcat(pathXml_ordered,'*.xml'));
numXML = numel(listFichXML);
strPatients(1,numXML)=struct('sex',[],'Age',[],'GDS',[],'diagnose',[],'ID',[],'visit',[],'imageUID',[],'EXAMDATE',[]);

%% PD Images
for i = 3:length(folders)
% Creacion de una carpeta para cada visita en cada paciente
    cont = 0;
    dwnames = [];
    subj_dir = [source_dir '/' folders(i).name];
 

    
% Busqueda de imagenes(MPRAGE_GRAPPA, MPRAGE_GRAPPA2, Sag_MPRAGE_GRAPPA, MPRAGE_T1_SAG, SAG_MPRAGE_GRAPPA, T1-anatomical)
    % MPRAGE
    strfolders = dir(subj_dir);
    folderNames={strfolders(3:end).name}';
    folderPaths=cell(size(strfolders));
    
    for k=3:length(strfolders)
        folderPaths{k}=fullfile(strfolders(k).folder,strfolders(k).name);
    end
    
    t1_dir1 = [subj_dir '/MPRAGE_GRAPPA'];
    t1_dir2 = [subj_dir '/Sag_MPRAGE_GRAPPA'];
    t1_dir3 = [subj_dir '/MPRAGE_GRAPPA2'];
    t1_dir4 = [subj_dir '/SAG_MPRAGE_GRAPPA'];
    t1_dir5 = [subj_dir '/MPRAGE_T1_SAG'];
    t1_dir6 = [subj_dir '/T1-anatomical'];
    t1_dir7 = [subj_dir '/SAG_T1_3D_MPRAGE'];
    t1_dir8 = [subj_dir '/SAG_T1_3DMPRAGE'];    
    if exist(t1_dir1,'dir')
        t1_subfolds1 = dir([t1_dir1 '/2*']); %Buscar carpetas que empiecen por 2 
        year1 = zeros(size(t1_subfolds1,1),1);
        fullpath1 = cell(size(t1_subfolds1,1),1);
        for j = 1:length(t1_subfolds1)
                year1(j) = str2num(strcat(t1_subfolds1(j).name(1:4))); %A�o en el que se toma la imagen
                fullpath1{j} = fullfile(t1_subfolds1(j).folder,t1_subfolds1(j).name); %Ruta de la ultima carpeta que contiene la imagen
        end

    else
        year1 = [];
        fullpath1 = {};
        
    end
    
    if exist(t1_dir2,'dir')
        t1_subfolds2 = dir([t1_dir2 '/2*']);
        year2 = zeros(size(t1_subfolds2,1),1);
        fullpath2 = cell(size(t1_subfolds2,1),1);
        for j = 1:length(t1_subfolds2)
                year2(j) = str2num(strcat(t1_subfolds2(j).name(1:4))); 
                fullpath2{j} = fullfile(t1_subfolds2(j).folder,t1_subfolds2(j).name);
        end

    else
        year2 = [];
        fullpath2 = {};
        
    end
    
    if exist(t1_dir3,'dir')
        t1_subfolds3 = dir([t1_dir3 '/2*']);
        year3 = zeros(size(t1_subfolds3,1),1);
        fullpath3 = cell(size(t1_subfolds3,1),1);
        for j = 1:length(t1_subfolds3)
                year3(j) = str2num(strcat(t1_subfolds3(j).name(1:4))); 
                fullpath3{j} = fullfile(t1_subfolds3(j).folder,t1_subfolds3(j).name);
        end

    else
        year3 = [];
        fullpath3 = {};
        
    end
    
     if exist(t1_dir4,'dir')
            t1_subfolds4 = dir([t1_dir4 '/2*']);
            year4 = zeros(size(t1_subfolds4,1),1);
            fullpath4 = cell(size(t1_subfolds4,1),1);
            for j = 1:length(t1_subfolds4)
                year4(j) = str2num(strcat(t1_subfolds4(j).name(1:4)));
                fullpath4{j} = fullfile(t1_subfolds4(j).folder,t1_subfolds4(j).name);
            end

        else
            year4 = [];
            fullpath4 = {};
     end
     
     if exist(t1_dir5,'dir')
            t1_subfolds5 = dir([t1_dir5 '/2*']);
            year5 = zeros(size(t1_subfolds5,1),1);
            fullpath5 = cell(size(t1_subfolds5,1),1);
            for j = 1:length(t1_subfolds5)
                year5(j) = str2num(strcat(t1_subfolds5(j).name(1:4)));
                fullpath5{j} = fullfile(t1_subfolds5(j).folder,t1_subfolds5(j).name);
            end

        else
            year5 = [];
            fullpath5 = {};
     end
    if exist(t1_dir6,'dir')
        t1_subfolds6 = dir([t1_dir6 '/2*']); %Buscar carpetas que empiecen por 2 
        year6 = zeros(size(t1_subfolds6,1),1);
        fullpath6 = cell(size(t1_subfolds6,1),1);
        for j = 1:length(t1_subfolds6)
                year6(j) = str2num(strcat(t1_subfolds6(j).name(1:4))); %A�o en el que se toma la imagen
                fullpath6{j} = fullfile(t1_subfolds6(j).folder,t1_subfolds6(j).name); %Ruta de la ultima carpeta que contiene la imagen
        end

    else
        year6 = [];
        fullpath6 = {};
    end  
    if exist(t1_dir7,'dir')
        t1_subfolds7 = dir([t1_dir7 '/2*']); %Buscar carpetas que empiecen por 2 
        year7 = zeros(size(t1_subfolds7,1),1);
        fullpath7 = cell(size(t1_subfolds7,1),1);
        for j = 1:length(t1_subfolds7)
                year7(j) = str2num(strcat(t1_subfolds7(j).name(1:4))); %A�o en el que se toma la imagen
                fullpath7{j} = fullfile(t1_subfolds7(j).folder,t1_subfolds7(j).name); %Ruta de la ultima carpeta que contiene la imagen
        end

    else
        year7 = [];
        fullpath7 = {};        
    end
        if exist(t1_dir8,'dir')
        t1_subfolds8 = dir([t1_dir8 '/2*']); %Buscar carpetas que empiecen por 2 
        year8 = zeros(size(t1_subfolds8,1),1);
        fullpath8 = cell(size(t1_subfolds8,1),1);
        for j = 1:length(t1_subfolds8)
                year8(j) = str2num(strcat(t1_subfolds8(j).name(1:4))); %A�o en el que se toma la imagen
                fullpath8{j} = fullfile(t1_subfolds8(j).folder,t1_subfolds8(j).name); %Ruta de la ultima carpeta que contiene la imagen
        end

    else
        year8 = [];
        fullpath8 = {};        
    end
    
    
    year = [year1;year2;year3;year4;year5;year6;year7;year8]; %Creacion de un vector con los posibles a�os
    fullpaths = [fullpath1;fullpath2;fullpath3;fullpath4;fullpath5;fullpath6;fullpath7;fullpath8]; %Creacion de un array con las rutas de las carpetas que contienen la imagen
   %Buscar archivos xml

    count=1;
    for j=1: numXML
       if(strcmp(folders(i).name,listFichXML(j).name(1:4)))
           xml_subj(count)=string(listFichXML(j).name);
           count=count+1;
       end
    end
% Copia de las imagenes en funcion del a�o de la carpeta    
    for m = 1:length(year)
        filename = strcat(pathXml_ordered, char(xml_subj(m)));
        theStruct = xml2struct(filename);

        % These parts of the struct are repeatedly used over the function.
        data1 = theStruct.idaxs.project.subject;
        visit = data1.visit.visitIdentifier.Text;
        
            
            if visit == 'Baseline'
                tmpdir=dir(fullpaths{m});
                subfolder=fullfile(tmpdir(3).folder,tmpdir(3).name);
                tmpdir=dir([subfolder '/*.nii']);
                FileIn=fullfile(tmpdir(1).folder,tmpdir(1).name); %Ruta de la imagen 
                FileOut=fullfile(pathId_all,strcat(folders(i).name,'_BL.nii')); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
                command = ['copy ' FileIn ' ' FileOut];
                system(command);
                
            elseif visit == 'Month 12'
                tmpdir2=dir(fullpaths{m});
                subfolder2=fullfile(tmpdir2(3).folder,tmpdir2(3).name);
                tmpdir2=dir([subfolder2 '/*.nii']);
                FileIn2=fullfile(tmpdir2(1).folder,tmpdir2(1).name); %Ruta de la imagen 
                FileOut2=fullfile(pathId_all,strcat(folders(i).name,'_V04.nii')); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
                command = ['copy ' FileIn2 ' ' FileOut2];
                system(command);
                
            elseif visit == 'Month 24'
                tmpdir3=dir(fullpaths{m});
                subfolder3=fullfile(tmpdir3(3).folder,tmpdir3(3).name);
                tmpdir3=dir([subfolder3 '/*.nii']);
                FileIn3=fullfile(tmpdir3(1).folder,tmpdir3(1).name); %Ruta de la imagen 
                FileOut3=fullfile(pathId_all,strcat(folders(i).name,'_V06.nii')); %Ruta en la que se copiarÃ¡ la imagen con un nuevo nombre en funciÃ³n de la visita
                command = ['copy ' FileIn3 ' ' FileOut3];
                system(command);
                
            elseif visit =='Month 48'
                tmpdir4=dir(fullpaths{m});
                subfolder4=fullfile(tmpdir4(3).folder,tmpdir4(3).name);
                tmpdir4=dir([subfolder4 '/*.nii']);
                FileIn4=fullfile(tmpdir4(1).folder,tmpdir4(1).name); %Ruta de la imagen 
                FileOut4=fullfile(pathId_all,strcat(folders(i).name,'_V10.nii')); %Ruta en la que se copiara la imagen con un nuevo nombre en funciÃ³n de la visita
                command = ['copy ' FileIn4 ' ' FileOut4];
                system(command);
            end    
    end        
end   
end
