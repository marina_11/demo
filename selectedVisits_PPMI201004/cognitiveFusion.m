function [PD_MCI,PD_HC]=cognitiveFusion

addpath('./selected_aux');

load('./data/Tresults_soloPD');

%mask_PD=PPMIApr20.APPRDX==1;
%PPMI_PD=PPMIApr20(mask_PD,:);
ID=unique(Tresults_soloPD.PATNO);
% PD_MCI_1=[];
% PD_MCI_2=[];
% PD_MCI_3=[];
mask_PD_MCI=false(size(Tresults_soloPD,1),1);
convertTime=zeros(size(Tresults_soloPD,1),1);
for i=1:length(ID)
    index=find(Tresults_soloPD.PATNO==ID(i));
    Tresults_soloPD_subj=Tresults_soloPD(index,:);
    cogstate=Tresults_soloPD_subj.cogstate; % 1(HC),2(MCI), 3(Dementia)
    MCI_testscores=Tresults_soloPD_subj.MCI_testscores; % 0 (HC), 1(MCI)
    np1cog=Tresults_soloPD_subj.NP1COG;  %0 (HC), 1(Slight), 2(Mild), 3 (Moderate), 4(Severate)
    EVENT_ID=Tresults_soloPD_subj.EVENT_ID;
    data_ID=[vis2double(EVENT_ID),cogstate,MCI_testscores,np1cog];
    %% Criteria 1
    criteria1=0;
    cogstate=cogstate(~isnan(cogstate));
    if(sum(cogstate>1)>0 && cogstate(end)>1)
        %PD_MCI_1=[PD_MCI_1;ID(i)];
        criteria1=1;
    end
    %% Criteria 2
    criteria2=0;
    MCI_testscores=MCI_testscores(~isnan(MCI_testscores));
    if(sum(MCI_testscores==1)>0 && MCI_testscores(end)==1)
        %PD_MCI_2=[PD_MCI_2;ID(i)];
        criteria2=1;
    end
    %% Criteria 3
    criteria3=0;
    np1cog=np1cog(~isnan(np1cog));
    if(sum(np1cog>0)>1 && np1cog(end)>0)
        %PD_MCI_3=[PD_MCI_2;ID(i)];
        criteria3=1;
    end
    %% Criteria join
    if((criteria1+criteria3)>1)
%         PD_MCI=[PD_MCI;ID(i)];
%         %ID(i)
%         data_ID
%         pause;
        mask_PD_MCI(index)=true;
        idx_first_cr1=find(data_ID(:,2)>1,1);
        idx_first_cr3=find(data_ID(:,4)>0,1);
        convertTime(index)=(data_ID(idx_first_cr1,1)+data_ID(idx_first_cr3,1))/2;

    elseif((criteria1+criteria2)>1)  %% a�ado criterio 2, MCI_testscores
        mask_PD_MCI(index)=true;
        idx_first_cr1=find(data_ID(:,2)>1,1);
        idx_first_cr2=find(data_ID(:,3)==1,1);
        convertTime(index)=(data_ID(idx_first_cr1,1)+data_ID(idx_first_cr2,1))/2;
  elseif((criteria3+criteria2)>1)  %% a�ado criterio 2, MCI_testscores
        mask_PD_MCI(index)=true;
        idx_first_cr3=find(data_ID(:,4)>0,1);
        idx_first_cr2=find(data_ID(:,3)==1,1);
        convertTime(index)=(data_ID(idx_first_cr3,1)+data_ID(idx_first_cr2,1))/2;

    else
        convertTime(index)=data_ID(end,1);
        
    end
    
        
        
end

Tresults_soloPD.convertTime=convertTime;
PD_MCI=Tresults_soloPD(mask_PD_MCI,:);
PD_HC =Tresults_soloPD(~mask_PD_MCI,:);
a=sum((PD_MCI.EVENT_ID=='BL'));
b= sum((PD_MCI.EVENT_ID=='V04'));
c= sum((PD_MCI.EVENT_ID=='V06'));
d= sum((PD_MCI.EVENT_ID=='V08'));
e= sum((PD_MCI.EVENT_ID=='V10'));
f= sum((PD_MCI.EVENT_ID=='V12'));
a1= sum((PD_HC.EVENT_ID=='BL'));
b1= sum((PD_HC.EVENT_ID=='V04'));
c1= sum((PD_HC.EVENT_ID=='V06'));
d1= sum((PD_HC.EVENT_ID=='V08'));
e1= sum((PD_HC.EVENT_ID=='V10'));
f1= sum((PD_HC.EVENT_ID=='V12'));
fprintf('HC %d\n', sum(~mask_PD_MCI));
fprintf('sujetos HC %d\n', length(unique(PD_HC.PATNO)));
fprintf('MCI %d\n', sum(mask_PD_MCI));
fprintf('sujetos MCI %d\n', length(unique(PD_MCI.PATNO)));
%% delete
rmpath('./selected_aux');

end





