function selectedVisitsPPMI_oct20(n_cohort)

cohort={'all','onlyPD'};
PPMI_MRI=MRI_PPMI_subjects(n_cohort);
PPMI_MRI=PPMI_MRI(~isnan(PPMI_MRI.PATNO_MRI),:);

%% More than one visit
ID=unique(PPMI_MRI.PATNO(PPMI_MRI.EVENT_ID_MRI~='BL'));
%ID=unique(PPMI_MRI.PATNO);
%%quitar de ID2 los que estan en ID1
%ID=setdiff(ID2, ID1);
mask_longVisits=[];
for i=1:length(ID)
    mask_longVisits=[mask_longVisits;find(PPMI_MRI.PATNO==ID(i))];
end
PPMI_MRI=PPMI_MRI(mask_longVisits,:);

%% New subjects: remove patients with some processed visits
mask_longVisits=[];
for i=1:length(ID)
    index=find(PPMI_MRI.PATNO==ID(i));
    if(sum(~isnan(PPMI_MRI.fsidbase(index)))==0)
        mask_longVisits=[mask_longVisits;index];
    end
end
PPMI_MRI=PPMI_MRI(mask_longVisits,:);

%% Info
mask_bsl=PPMI_MRI.EVENT_ID=='BL';
clc;
fprintf('New subjects: %d = %d (Control) + %d (Converters)\n',sum(mask_bsl),...
    sum(PPMI_MRI.Convert(mask_bsl)==0),sum(PPMI_MRI.Convert(mask_bsl)));
fprintf('New visits: %d = %d (Control) + %d (Converters)\n',size(PPMI_MRI,1),...
    sum(PPMI_MRI.Convert==0),sum(PPMI_MRI.Convert));

%% Visit list
ID=unique(PPMI_MRI.PATNO);
for i=1:length(ID)    
    index= find(PPMI_MRI.PATNO==ID(i));
    if(PPMI_MRI.Convert(index(1))==0)
        group='Control';
    else
        group='Converter';
    end
    fprintf('Subject %d (%s) visit to download:',ID(i),group);
    for j=1:length(index)
        fprintf('%s,',string(PPMI_MRI.EVENT_ID(index(j))));
    end
    fprintf('\n');
end

end



