function PD_MRI=MRI_PPMI_subjects(n_cohort)
clc;
cohort={'all','onlyPD'};
%% data
if(strcmp(cohort{n_cohort},'all'))
    load('./data/PPMIApr20_5Years','PPMIApr20');    
    mask_PD=PPMIApr20.APPRDX==1;
    mask_HC=PPMIApr20.APPRDX==2;
    PD_MCI=PPMIApr20(mask_PD,:);
    PD_HC=PPMIApr20(mask_HC,:);
elseif(strcmp(cohort{n_cohort},'onlyPD'))
    [PD_MCI,PD_HC]=cognitiveFusion;
else
end
load('./data/MRI_PPMISep20','MRI_PPMISep20');
load('./data/Tresults_507v_175s_170820','Tresults')

%% MRI index
PD_MCI.Convert=true(size(PD_MCI,1),1);
PD_HC.Convert=false(size(PD_HC,1),1);
PD=[PD_MCI;PD_HC];
index=zeros(size(PD,1),1);
ID=unique(PD.PATNO);
for i=1:length(ID)
    index_MRI=find(MRI_PPMISep20.PATNO==ID(i));
    if(isempty(index_MRI)==0)
        index_clinical=find(PD.PATNO==ID(i));
        visit_clinical=PD.EVENT_ID(index_clinical);
        visit_MRI=MRI_PPMISep20.EVENT_ID(index_MRI);
        for j=1:length(visit_MRI)
            index_visit=find(visit_clinical==visit_MRI(j));
            if(length(index_visit)==1)
                index(index_clinical(index_visit))=index_MRI(j);
            else
                fprintf('Error matching in visit %s from %d subject.\n',...
                    string(visit_MRI(j)),ID(i));
            end
        end 
    end
        
end
%% Tresults index
index_Tr=zeros(size(PD,1),1);
for i=1:length(ID)
    index_MRI=find(Tresults.fsidbase==ID(i));
    if(isempty(index_MRI)==0)
        index_clinical=find(PD.PATNO==ID(i));
        visit_clinical=PD.EVENT_ID(index_clinical);
        visit_MRI=Tresults.EVENT_ID(index_MRI);
        for j=1:length(visit_MRI)
            index_visit=find(visit_clinical==visit_MRI(j));
            if(length(index_visit)==1)
                index_Tr(index_clinical(index_visit))=index_MRI(j);
            else
                fprintf('Error matching in visit %s from %d subject.\n',...
                    string(visit_MRI(j)),ID(i));
            end
        end 
    end
        
end

%% join

MRI_table_aux=table;
PATNO_MRI=nan(size(PD,1),1);
PATNO_MRI(index>0)=MRI_PPMISep20.PATNO(index(index>0));
MRI_table_aux.PATNO_MRI=PATNO_MRI;
EVENT_ID_MRI(index>0)=MRI_PPMISep20.EVENT_ID(index(index>0));
MRI_table_aux.EVENT_ID_MRI=EVENT_ID_MRI';

MRI_table_aux2=table;
fsidbase=nan(size(PD,1),1);
fsidbase(index_Tr>0)=Tresults.fsidbase(index_Tr(index_Tr>0));
MRI_table_aux2.fsidbase=fsidbase;
EVENT_ID_Tresults=categorical;
EVENT_ID_Tresults(1:size(PD,1))='undefined';
EVENT_ID_Tresults(index_Tr>0)=Tresults.EVENT_ID(index_Tr(index_Tr>0));
MRI_table_aux2.EVENT_ID_Tresults=EVENT_ID_Tresults';

PD_MRI=[PD,MRI_table_aux,MRI_table_aux2];

% MRI_PPMISep20.Properties.VariableNames{'PATNO'}='PATNO_MRI';
% MRI_PPMISep20.Properties.VariableNames{'EVENT_ID'}='EVENT_ID_MRI';

% MRI_tmp=MRI_PPMISep20(1:size(PD,1),[1,3,4]);
% MRI_tmp(index>0,:)=MRI_PPMISep20(index(index>0),[1,3,4]);
% MRI_tmp(index==0,1:2)=nan;
% MRI_tmp(index==0,end)='ST';

% PD_MRI=[PD(index>0,:),MRI_PPMISep20(index(index>0),:)];

%% checking
PD_check=PD_MRI(isnan(PD_MRI.PATNO_MRI)==0,:);
ID=unique(PD_check.PATNO);
for i=1:length(ID)
    visit_clinical=PD_check.EVENT_ID    (PD_check.PATNO==ID(i));
    visit_MRI=     PD_check.EVENT_ID_MRI(PD_check.PATNO_MRI==ID(i));
    diff_visits=visit_clinical~=visit_MRI;
    if(sum(diff_visits))
        fprintf('Subject ID %d does not match between clinical and MRI data\n',ID(i));
    end
    
end

PD_check=PD_MRI(isnan(PD_MRI.fsidbase)==0,:);
ID=unique(PD_check.PATNO);
for i=1:length(ID)
    visit_clinical=PD_check.EVENT_ID    (PD_check.PATNO==ID(i));
    visit_MRI=     PD_check.EVENT_ID_Tresults(PD_check.fsidbase==ID(i));
    diff_visits=visit_clinical~=visit_MRI;
    if(sum(diff_visits))
        fprintf('Subject ID %d does not match between clinical and MRI data\n',ID(i));
    end
    
end

%% delete

end