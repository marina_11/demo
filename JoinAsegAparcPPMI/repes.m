function TresultsNew=repes(Tresults)
delLidia=0;
delAlvaro=0;
%% mascara para eliminar visitas repetidas de Tresults
mask=zeros(size(Tresults,1),1);
%cojo paciente y visita de alvaro y lo busco en los lotes de lidia
%si esta, elimino donde tenga menos visitas
alvaro=importdata('./data/V173_69s/clinicaldata_long_V173.dat'); %avlaro
lidia1=importdata('./data/lidia/clinicaldata_long_conversores_precl_V19.dat');
lidia2=importdata('./data/lidia/clinicaldata_long_conversores_precl_V126.dat');
lidia3=importdata('./data/lidia/clinicaldata_long_conversores_precl_V60.dat');
lidia4=importdata('./data/lidia/clinicaldata_long_conversores_precl_V160.dat');
for i=2 : length(alvaro.textdata) %repes entre alvaro y lidia
    fsid=char((alvaro.textdata(i,1))); %sujeto mas visita, lo busco en lidia 
    idxlidia1=strfind(string(lidia1.textdata(:,1)),fsid);
    idxlidia2=strfind(string(lidia2.textdata(:,1)),fsid);
    idxlidia3=strfind(string(lidia3.textdata(:,1)),fsid);
    idxlidia4=strfind(string(lidia4.textdata(:,1)),fsid);
        a1 = cellfun(@(x)isequal(x,1),idxlidia1);
        i1=find(a1,1);
        a2 = cellfun(@(x)isequal(x,1),idxlidia2);
        i2=find(a2,1);
        a3 = cellfun(@(x)isequal(x,1),idxlidia3);
        i3=find(a3,1);
        a4 = cellfun(@(x)isequal(x,1),idxlidia4);
        i4=find(a4,1);
      if( ~isempty(i1) || ~isempty(i2) || ~isempty(i3)|| ~isempty(i4))
        % a=a+1; %6 repetidos en el lote v126
         %%comprobar si hay mas visitas en alvaro o en lidia 
            numalvaro=sum(cell2mat(strfind(string(alvaro.textdata(:,2)), fsid(1:4))));
            numlidia1=sum(cell2mat(strfind(string(lidia1.textdata(:,2)), fsid(1:4))));
            numlidia2=sum(cell2mat(strfind(string(lidia2.textdata(:,2)), fsid(1:4))));
            numlidia3=sum(cell2mat(strfind(string(lidia3.textdata(:,2)), fsid(1:4))));
            numlidia4=sum(cell2mat(strfind(string(lidia4.textdata(:,2)), fsid(1:4))));
        if(numlidia1 > numalvaro ||numlidia2 > numalvaro || numlidia3 >numalvaro || numlidia4 >numalvaro)
            %%elimino visita de alvaro, busco la de alvaro en la tabla TMarkers
            n=ismember(Tresults.fsid,fsid );
            index=find(n, 1, 'last');
            mask(index)=1;
            delAlvaro=delAlvaro+1;
        else
            %%elimino visita de lidia
            %%como Tmarkers une primero la de lidia y luego alvaro, para
            %%eliminar lidia sera la primera en aparecer
            n=ismember(Tresults.fsid,fsid );
            index=find(n, 1, 'first');
            mask(index)=1;
            delLidia=delLidia+1;
        end
      end
end
%% repetidas entre las de lidia
for m=2 : length(lidia1.textdata) %repes entre alvaro y lidia
    fsid=char((lidia1.textdata(m,1)));
    idxlidia2=strfind(string(lidia2.textdata(:,1)),fsid);
    idxlidia3=strfind(string(lidia3.textdata(:,1)),fsid);
    idxlidia4=strfind(string(lidia4.textdata(:,1)),fsid);
        a2l = cellfun(@(x)isequal(x,1),idxlidia2);
        i2l=find(a2l,1);
        a3l = cellfun(@(x)isequal(x,1),idxlidia3);
        i3l=find(a3l,1);
        a4l = cellfun(@(x)isequal(x,1),idxlidia4);
        i4l=find(a4l,1);
      if(~isempty(i2l) || ~isempty(i3l)|| ~isempty(i4l))
            numlidia1l=sum(cell2mat(strfind(string(lidia1.textdata(:,2)), fsid(1:4))));
            numlidia2l=sum(cell2mat(strfind(string(lidia2.textdata(:,2)), fsid(1:4))));
            numlidia3l=sum(cell2mat(strfind(string(lidia3.textdata(:,2)), fsid(1:4))));
            numlidia4l=sum(cell2mat(strfind(string(lidia4.textdata(:,2)), fsid(1:4))));
        if(numlidia2l > numlidia1l ||numlidia3l >numlidia1l || numlidia4l >numlidia1l)
            n=ismember(Tresults.fsid,fsid );
            index=find(n, 1, 'first');
            mask(index)=1;
        else
            n=ismember(Tresults.fsid,fsid );
            index=find(n, 1, 'last');
            mask(index)=1;
        end
                    delLidia=delLidia+1;
      end
end
%%se aplica la mascara para eliminar
TresultsNew=table;
fprintf('elementos repetidos %i\n, eliminados lidia %i, eliminados alvaro %i\n', sum(mask), delLidia, delAlvaro);
for g=1:size(mask,1)
    if mask(g)==0
        TresultsNew(end+1,:)=Tresults(g,:);
    end
end
end