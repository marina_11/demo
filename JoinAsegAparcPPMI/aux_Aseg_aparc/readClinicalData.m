function Tdec=readClinicalData(fname)

Qdec = fReadQdec(fname);
Tdec=cell2table(Qdec(2:end,:));
varname=cellstr(Qdec(1,:));
varname{2}='fsidbase';
Tdec.Properties.VariableNames=varname;
Tdec.years=str2double(Tdec.years);
Tdec.Age=str2double(Tdec.Age);
Tdec.GDS=str2double(Tdec.GDS);
Tdec.fsidbase=str2double(Tdec.fsidbase);
Tdec.ID=str2double(Tdec.ID);
Tdec.imageUID=str2double(Tdec.imageUID);


end