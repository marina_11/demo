function TMarkers=asegAparc2table(data_path,data_name)   
%% Long
    fdemographic = strcat(data_path,'/',data_name);
    faseg = strcat(data_path,'/','aseg_long.',data_name);
    faparc_lh = strcat(data_path,'/','aparc_long_lh.',data_name);
    faparc_rh = strcat(data_path,'/','aparc_long_rh.',data_name);
    
    
    %% Read demographic and clinical data
    Tdec = readClinicalData(fdemographic);
    
    %% Read aseg+aparc table
    TMarkers = convertAsegAparc2Table(faseg,faparc_lh,faparc_rh);
    TMarkers = [Tdec,TMarkers];
end


