function checking_NII_PPMIMERGE

clc;
%Tresults=nuevo_join;
Tresults=load('Tresults_1113v_198s');
Tresults=Tresults.Tresults_1113v_198s;
Tresults = sortrows(Tresults,'PATNO','ascend');
%% All visits from a subject
ID=Tresults.PATNO;
ID_subjs=unique(ID);
for i=1:length(ID_subjs)
    %% Coherence Tmarkers
    index=find(Tresults.PATNO==ID_subjs(i));
    if(issorted(index)==0)
        fprintf('No sorted visits from ID %d\n',ID_subjs(i));
    end
    follow_visit=index(2:end)-index(1:end-1);
    mask_follow_visit=follow_visit~=1;
    if(sum(mask_follow_visit)>0)
        fprintf('There are jumps among visits from ID %d\n',ID_subjs(i));
    end
    
    years=Tresults.YEAR(index);
    if(issorted(years)==0)
        fprintf('No sorted years from ID %d\n',ID_subjs(i));
    end
    if(years(1)>0)
        fprintf('First visit is not baseline from ID %d\n',ID_subjs(i));
    end 
    
    
end

%% BSL VISCODE
mask_YEAR_BSL_warn = (Tresults.years ==0) & (Tresults.YEAR ~= 0); %%%%REPASAR

fsidbase=Tresults.PATNO(mask_YEAR_BSL_warn);
for i=1:length(fsidbase)
    fprintf('ID %d with baseline visit in years equal zero but YEAR is not baseline\n',...
        fsidbase(i));
end

%% VISCODE & years
year=zeros(size(Tresults,1),1);
for i=1:size(Tresults,1)
    year(i)=visit2num(Tresults.YEAR(i));
end

diff_month=abs(Tresults.years*12-year);
if(sum(diff_month>6)>0)
    fprintf('Number of visits different between years and YEAR: %d\n',...
        sum(diff_month>6));
    idx=find(diff_month>6);
    for i=1:length(idx)
        fprintf('%d\n',idx(i));
    end
end


end



function visit=visit2num(YEAR) %%year
vis=char(YEAR);
if(vis(1)=='b')
    visit=0;
else
    visit=str2double(vis(2:end));
end

end

