function Tresults_1113v_198s=nuevo_join
addpath('./external/lme/');
addpath('./external/lme/Qdec/');
addpath('./aux_Aseg_aparc');
%marina
T68=asegAparc2table('./data/68V_23s/','clinicaldata_long_68V_23s.dat');
%alvaro
T173=asegAparc2table('./data/V173_69s/','clinicaldata_long_V173.dat');
%lidia
T19=asegAparc2table('./data/lidia/','clinicaldata_long_conversores_precl_V19.dat');
T119=asegAparc2table('./data/lidia/','clinicaldata_long_conversores_precl_V126.dat');
T60=asegAparc2table('./data/lidia/','clinicaldata_long_conversores_precl_V60.dat');
T160=asegAparc2table('./data/lidia/','clinicaldata_long_conversores_precl_V160.dat');

Tresults=[T68; T173; T19; T119; T60; T160];

load('./data/PPMIApr20_5Years');

ID=unique(Tresults.fsidbase);
index=ismember(PPMIApr20.PATNO, ID);
TMarkers=PPMIApr20(index,:); %% SE TRABAJA SOBRE ESTA

sz=[size(TMarkers,1), size(Tresults, 2)];
Tresults_nan=(NaN(sz));
Tresults_nan=array2table(Tresults_nan);
Tresults_nan.Properties.VariableNames=Tresults.Properties.VariableNames;
vector_nan=NaN([1 74]);
Results=[TMarkers, Tresults_nan];

%% antes de unir Results y Tresults, quitar las repes de Tresults
TresultsNew=repes(Tresults);


E_ID=extractAfter(string(TresultsNew.fsid),5); %codigo visitas
index_Results=zeros(size(Results,1),1);
index_Tresults=zeros(size(TresultsNew,1),1);
% for i=1: (size(TresultsNew,1))
%      index_T=find(TresultsNew.fsidbase(i)==Results.PATNO & strcmp(string(Results.EVENT_ID),E_ID(i)));
%     if(length(index_T) == 1)
%         index_Tresults(i)=index_T;       
%     end
% end

for j=1: (size(Results,1))
    b=num2str(Results.PATNO(j));
    c=char(Results.EVENT_ID(j));
    a=strcat(b,'_',c);
    h=char(TresultsNew.fsid);
     [index_R, index_Tr]=ismember(a, char(TresultsNew.fsid), 'rows');
     %index_Tr=strfind(strcat(TresultsNew.fsid, strcat(num2str(Results.PATNO(index_R)),'_', char(Results.EVENT_ID(index_R)))));
     if(length(index_R) == 1)
        index_Results(j)=index_R;       
    end
    if(length(index_Tr) == 1)
        index_Tresults(j)=index_Tr;       
    end
end
a=index_Results; %mascara que te da posiciiones
b=index_Tresults(index_Tresults>0);
p=1;
k=1;
while (p<sz(1)+1) 
        if(a(p)==0)
            T{p,:}=[TMarkers(p, :) Tresults_nan(1,:)];
         else 
            T{p,:}=[TMarkers(p, :) TresultsNew(b(k),:)]; 
            k=k+1;
        end
  p=p+1;
end   

TResults_all_vistis=table;
for m=1: size(T,1)
    TResults_all_vistis=[TResults_all_vistis;T{m,1}];
end

Tresults_1113v_198s=parche(TResults_all_vistis);

a=((Tresults_1113v_198s.YEAR-Tresults_1113v_198s.years)>1 |(Tresults_1113v_198s.YEAR-Tresults_1113v_198s.years)<-1);
rmpath('./external/lme/');
rmpath('./external/lme/Qdec/');
rmpath('./aux_Aseg_aparc');

Tresults_soloPD=Tresults_1113v_198s(Tresults_1113v_198s.APPRDX==1, :);
end





