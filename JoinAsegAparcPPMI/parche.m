function [TMarkers]=parche(TMarkers)

for i=2:size(TMarkers.years,1)
    if TMarkers.years(i)==0 && (strcmp(TMarkers.visit(i),'Baseline')~=1)
        if(TMarkers.PATNO(i)==TMarkers.PATNO(i-1))
            if (strcmp(TMarkers.visit(i),'Month_12'))
                TMarkers.years(i)=1;
            elseif (strcmp(TMarkers.visit(i),'Month_24'))
                 TMarkers.years(i)=2;
            elseif (strcmp(TMarkers.visit(i),'Month_48'))
                 TMarkers.years(i)=4;
            end
            if(strcmp(TMarkers.visit(i+1),'Month_24') && TMarkers.years(i+1)<=0 )
                TMarkers.years(i+1)=TMarkers.years(i+1)+2;
            elseif(strcmp(TMarkers.visit(i+1),'Month_24') && TMarkers.years(i+1)<=1 )
                TMarkers.years(i+1)=TMarkers.years(i+1)+1;
            end
             if(strcmp(TMarkers.visit(i+3),'Month_48') && TMarkers.years(i+3)<=2 )
                TMarkers.years(i+3)=TMarkers.years(i+3)+2;
             elseif(strcmp(TMarkers.visit(i+3),'Month_48') && TMarkers.years(i+3)<3 )
                TMarkers.years(i+3)=TMarkers.years(i+3)+1;
            end
        
    end

end
end