function samples_by_visits_PD

 %load('./data/Tresults_PPMI_352v_128s', 'Tresults');

% load('./data/Tresults_PPMI_367v_130s', 'Tresults');

% load('./data/Tresults_PPMI_368v_130s', 'Tresults');

load('./data/Tresults_507v_175s_170820', 'Tresults');

viscode_num=zeros(size(Tresults,1),1);
for i=1:size(Tresults,1)
    viscode_num(i)=visit2num(grp2idx(Tresults.VISCODE(i)));    
end

type_visit=unique(viscode_num);
visit_NC=0;
visit_PD=0;
group=double(strcmp(Tresults.diagnose,'PD'));

for i=1:length(type_visit)
    tmp_NC=sum(viscode_num==type_visit(i) & group==0);
    tmp_PD=sum(viscode_num==type_visit(i) & group==1);
    visit_NC=visit_NC+tmp_NC;
    visit_PD=visit_PD+tmp_PD;
    
    
    fprintf('Visit %d: %d (NC) %d (PD)\n',type_visit(i),...
        tmp_NC,tmp_PD);
end

fprintf('Visit all: %d (NC) %d (PD)\n',visit_NC,visit_PD);

end


function visit=visit2num(VISCODE)
if(VISCODE==1)
    visit=0;
elseif(VISCODE==2)
    visit=1;
elseif(VISCODE==3)
    visit=2;
elseif(VISCODE==5 || VISCODE ==4)
    visit=4;
else        
    visit=-1;
end


end

