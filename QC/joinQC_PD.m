function [corr_QC_all,vol_QC_all]=joinQC_PD
load('./data/mask_repes.mat'); %598
%% QC partial
%%UNIRLAS EN EL MISMO ORDEN QUE EN JOIN PARA QUE SIRVA LA MASCARA
%V68
load('./data/FS/QC_long_V68_S23','corr_QC','vol_QC');
corr_QC_all=corr_QC;
vol_QC_all=vol_QC;

%V173
load('./data/FS/QC_long_V173_S69','corr_QC','vol_QC');
corr_QC_all=[corr_QC_all;corr_QC];
vol_QC_all=[vol_QC_all;vol_QC];

%V19
load('./data/FS/QC_long_V17_19','corr_QC','vol_QC');
corr_QC_all=[corr_QC_all;corr_QC];
vol_QC_all=[vol_QC_all;vol_QC];


%V119
load('./data/FS/QC_long_V119','corr_QC','vol_QC');
corr_QC_all=[corr_QC_all;corr_QC];
vol_QC_all=[vol_QC_all;vol_QC];

%V60
load('./data/FS/QC_long_V60','corr_QC','vol_QC');
corr_QC_all=[corr_QC_all;corr_QC];
vol_QC_all=[vol_QC_all;vol_QC];

%V160=155
load('./data/FS/QC_long_V155','corr_QC','vol_QC');
corr_QC_all=[corr_QC_all;corr_QC];
vol_QC_all=[vol_QC_all;vol_QC];
%%total
corr_QC_all = sortrows(corr_QC_all,'fsidbase','ascend');
vol_QC_all = sortrows(vol_QC_all,'fsid','ascend');
%% remove 4019_BL
vol_QC_all(524,:)=[];
%corr_QC_all(30,:)=[];

[corr_QC_all, vol_QC_all]=removeExtraVisits(corr_QC_all,vol_QC_all); %%%%%%%%%%%%%%


%% Checking
% load('./data/QC_long_ADNI_671v_215s','corr_QC','vol_QC')
% diff_vol=sum(abs(table2array(vol_QC(:,2:end))-table2array(vol_QC_all(:,2:end))));
% diff_corr=sum(abs(table2array(corr_QC(:,2:end))-table2array(corr_QC_all(:,2:end))));
% 
% if(sum(diff_corr)>0 || sum(diff_vol)>0)   
%     warning('there are differents');
% end

end




