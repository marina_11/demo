% Contenidos del control de calidad
% Este ejemplo utiliza comparaci�n entre sujetos NC vs NC_converters
%
% 1. QC_FS_PPMI: Calcula las medidas de intensidad y etiquetamiento con las im�genes del sujeto.
%    Leer las im�genes del sujeto y calcular la correlaci�n de intensidad
%    y el dice de los etiquetamientos. Utiliza QC_FS_ADNI.m.
%    Ejecutable desde linux, ya que emplea Freesufer Subject Dir.
%
% 2. QC_corr_dice_scans: Leer las medidas de correlaci�n y dice de QC_FS_ADNI.m y de Tresults
%    procedente de aseg y aparc. Utiliza QC_corr_dice_scans.m
%    Visualiza en Fig. 1 medidas entre sujetos y en Fig. 2 entre visitas.
%
%
% 3. comparative_BernalData_PD: Comparativa entre los marcadores de volumen hipocampal normalizado y
%    espesor de la corteza entorrinal dados por Bernal y los calculados por
%    el procesamiento longitudinal nuestro. Se muestra las trayectorias
%    de los dos marcadores y los valores medios al inicio del estudio y
%    la atrofia anual. Utiliza comparative_BernalData_NC.m
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliar
% samples_by_visits: numero de visitas por grupo y mes
% joinQC_PD: unir QC de diferentes partidas experimentales
%
%  20/02/20