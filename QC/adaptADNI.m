function [corr_QC_all,vol_QC_all] = adaptADNI(corr_QC,vol_QC,corr_QC_all,vol_QC_all)
%     fsid=TMarkers.fsid;
%     fsidbase=string(zeros(size(fsid,1),1));
%     TMarkers.visit=TMarkers.diagnose;
%     for i=1:size(fsid)
%         fsid_aux=char(fsid(i));
%         fsidbase(i)=string(strcat(fsid_aux(1:3),fsid_aux(7:10)));
%         TMarkers.diagnose(i)=cellstr('Control');
%         if TMarkers.years(i)==0
%             TMarkers.visit(i)=cellstr('Baseline');
%         elseif TMarkers.years(i)>0.75 && TMarkers.years(i)<1.25
%             TMarkers.visit(i)=cellstr('Month_12');
%         elseif TMarkers.years(i)>1.75 && TMarkers.years(i)<2.25
%             TMarkers.visit(i)=cellstr('Month_24');
%         elseif TMarkers.years(i)>3.75 && TMarkers.years(i)<4.25
%             TMarkers.visit(i)=cellstr('Month_48');
%         else
%             TMarkers.visit(i)=cellstr('Out_of_study');
%         end
%     end
%     TMarkers.fsidbase=double(fsidbase);
%     TMarkers(:,[6, 8, 10, 11])=[];
%     TMarkers.ID=TMarkers.fsidbase;
%     mask_visit=~strcmp(TMarkers.visit,'Out_of_study');
%     TMarkers=TMarkers(mask_visit,:);
%     TMarkers.Id=TMarkers.ID;
%     TMarkers=TMarkers(:,[1:3 5 4 6:7 74 10 8 75 11:73 9]);
fsid=string(corr_QC.fsidbase);
fsidbase=string(zeros(size(fsid,1),1));
for i=1:size(fsid)
        fsid_aux=char(fsid(i));
        fsidbase(i)=string(strcat(fsid_aux(1:3),fsid_aux(7:10)));
        
end
corr_QC.fsidbase=char(fsidbase);
fsid=string(corr_QC_all.fsidbase);
fsidbase=string(zeros(size(fsid,1),1));
for i=1:size(corr_QC_all.fsidbase,1)
    fsid_aux=fsid(i);
    fsidbase(i)=string(strcat('000',fsid_aux));
end
corr_QC_all.fsidbase=char(fsidbase);

%%Remove extra visits
load('./data/Tresults_633v_215s_18082020','Tresults');

fsid=string(vol_QC.fsid);
fsidbase=string(zeros(size(fsid,1),1));

for i=1:size(fsid)
        fsid_aux=char(fsid(i));
        fsidbase(i)=string(fsid_aux(1:23));
%         if(~find(Tresults.fsid==vol_QC.fsid(i)))
%             mask_fsid(i)=0;
%         end
end
vol_QC.fsid=char(fsidbase);
vol_QC=vol_QC([2 1:164],:);
fsid=string(vol_QC.fsid);
mask_fsid=zeros(size(fsid,1),1);

for i=1:(size(mask_fsid,1)-1)
   for j=508:size(Tresults.fsid,1)
    if(strcmp(string(Tresults.fsid(j)),fsid(i)))
        mask_fsid(i)=1;
      
    end
   end 
end


mask_fsid=logical(mask_fsid);
vol_QC=vol_QC(mask_fsid,:);

corr_QC_all=[corr_QC_all;corr_QC];
corr_QC_all.ni=calc_ni;
vol_QC_all=[vol_QC_all;vol_QC];
end