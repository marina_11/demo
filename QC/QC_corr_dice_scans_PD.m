% function QC_corr_dice_scans_PD
% Input: Tresults, QC measurements from
% Output: Pearson correlation vs dice from a MRI subject scans (figure 1)
%         Hippocampal and cortex volumes from MRI vs Tresults.           
%% data
 Tresults=load('./data/Tresults_1113v_198s.mat'); %alvaro
[corr_QC,vol_QC]=joinQC_PD;
%% Data check
Tresults=Tresults.Tresults_1113v_198s;
Tresults = sortrows(Tresults,'PATNO','ascend');
diff=false(size(Tresults,1),1);
fsid=string(vol_QC.fsid);
A=~ismissing(Tresults.fsidbase);% mascara de missing
Tresults=Tresults(A,:);
for i=1:size(Tresults,1)
    diff(i)=strcmp(Tresults.fsid(i),fsid(i));
end
if(sum(diff)~=size(Tresults,1))
    warning('Tresults and vol-QC with different ordering\n');
end

ni=calc_ni;
a=corr_QC.ni;
if(sum(abs(ni-corr_QC.ni))>0)
    warning('Tresults and corr-QC with different ni\n');
end

%% Pearson vs dice
close all;

QC=[corr_QC.coefImg_Hipp,corr_QC.coefLabel_Hipp,corr_QC.coefImg_Cortex,...
    corr_QC.coefLabel_Cortex];

figure(1);
mask_bsl=Tresults.years==0;
% plotQC(QC,string(Tresults.fsid(mask_bsl)),Tresults.DIAGNOSE(mask_bsl));
plotQC(QC,string(Tresults.PATNO(mask_bsl)),double(strcmp(Tresults.diagnose(mask_bsl),'PD')));

%% Correlation between stats and masks of the aseg
figure(2);
subplot(2,2,1);plot(Tresults.LHippVol,vol_QC.LHipp_vol,'+');
xlabel('LHV (stats) mm3'); ylabel('LHV (aseg) mm3'); 
subplot(2,2,2);plot(Tresults.RHippVol,vol_QC.RHipp_vol,'+');
xlabel('RHV (stats) mm3'); ylabel('RHV (aseg) mm3'); 
subplot(2,2,3);plot(Tresults.LCortVol,vol_QC.LCortex_vol,'+');
xlabel('L CortexVol (stats) mm3'); ylabel('L CortexVol (aseg) mm3'); 
subplot(2,2,4);plot(Tresults.RCortVol,vol_QC.RCortex_vol,'+');
xlabel('R CortexVol (stats) mm3'); ylabel('R CortexVol (aseg) mm3'); 
%end


function plotQC(QC,fsidbase,diagn)
subplot(1,2,1);
hold on;
plot(QC(diagn==0,1),QC(diagn==0,2),'r+',QC(diagn==1,1),QC(diagn==1,2),'go');
axis([0 1 0 1]);
index_outliers_Hipp=find(QC(:,1)<.7 & QC(:,2)<.7);
% if(isempty(index_outliers_Hipp)==0)
%     for i=1:length(index_outliers_Hipp)
%         text(QC(index_outliers_Hipp(i),1),QC(index_outliers_Hipp(i),2),fsidbase(index_outliers_Hipp(i),:),'Interpreter','none');
%     end
% end

hold off;
legend('NC','PD');
xlabel('Intensity correlation coefficients');
ylabel('Labeling dice coefficients');
title('Lowest correlations between pairs of scans from each subject for the hippocampus');

subplot(1,2,2);
hold on;
plot(QC(diagn==0,3),QC(diagn==0,4),'r+',QC(diagn==1,3),QC(diagn==1,4),'go');

axis([0 1 0 1]);
index_outliers_Cortex=find(QC(:,3)<.7 & QC(:,4)<.7);
% if(isempty(index_outliers_Cortex)==0)
%     for i=1:length(index_outliers_Cortex)
%         text(QC(index_outliers_Cortex(i),3),QC(index_outliers_Cortex(i),4),...
%             fsidbase(index_outliers_Cortex(i),:),'Interpreter','none');
%     end
% end

hold off;
axis([0 1 0 1]);
legend('NC','PD');
xlabel('Intensity correlation coefficients');
ylabel('Labeling dice coefficients');
title('Lowest correlations between pairs of scans from each subject for the cortex');
end

