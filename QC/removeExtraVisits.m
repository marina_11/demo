function [corr_QC,vol_QC]=removeExtraVisits(corr_QC,vol_QC)
Tresults=load('./data/Tresults_575v_198s');
Tresults=Tresults.ans;
mask_corr=ones(size(corr_QC.fsidbase,1),1);
mask_vol=ones(size(vol_QC.fsid,1),1);

fsidbase=string(corr_QC.fsidbase);

for i=2:size(fsidbase,1)
    if(strcmp(fsidbase(i),fsidbase(i-1)))
        mask_corr(i)=0;
    end
end
mask_corr=logical(mask_corr);
corr_QC=corr_QC(mask_corr,:);
fsid=vol_QC.fsid;
for i=1:size(fsid,1)
    if (strlength(fsid(i))>8)
        fsid_aux=char(fsid(i));
        if (fsid_aux(6)=='B')
            fsid(i)=string(fsid_aux(1:7));
        else
            fsid(i)=string(fsid_aux(1:8));
        end
    end
end
vol_QC.fsid=fsid;
for i=2:size(fsid,1)
    if(strcmp(fsid(i),fsid(i-1)))
        mask_vol(i)=0;
    end
end
mask_vol=logical(mask_vol);
vol_QC=vol_QC(mask_vol,:);
fsid=vol_QC.fsid;
fsidTresults=string(Tresults.fsid);
% for i=1:size(fsidTresults)
%     if(~strcmp(fsidTresults(i),fsid(i)))
%         vol_QC(i,:)=[];
%         fsid(i,:)=[];
%     end
% end
%hay que recalcular ni
a=extractBefore(string(vol_QC.fsid),'_');
fsidbase=string(corr_QC.fsidbase);
for i=1:size (corr_QC,1)

    b=length(find(strcmp(fsidbase(i), a)));
    corr_QC.ni(i)=b;
end

end