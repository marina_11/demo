 function ni=calc_ni

Tresults=load('./data/Tresults_1113v_198s.mat'); %alvaro
Tresults=Tresults.Tresults_1113v_198s;
A=~ismissing(Tresults.fsidbase);% mascara de missing
Tresults=Tresults(A,:);
tabla=Tresults;
tabla=sortrows(tabla,'PATNO','ascend');
id=tabla.PATNO(1);
n=0;
j=0;
for i=1:height(tabla)
    if(tabla.PATNO(i)==id)
       n=n+1;
    else
        j=j+1;
        ni(j,1)=n;
        n=1;
        id=tabla.PATNO(i);
    end
end
j=j+1;
ni(j,1)=n;
end