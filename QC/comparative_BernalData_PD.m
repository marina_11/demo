 function comparative_BernalData_PD
addpath('./external/lme/univariate'); %lme_lowessPlot
close all;

%% data
% PD
Tresults=load('./data/Tresults_68v_23s');
Tresults=Tresults.ans;
load('./data/ADNI791_Hipp_and_Entorh.mat','X_Hipp','Y','group','ni');

%% Checking NHV and ECT in relation to Bernal data
NHV_all=(Y(:,1)+Y(:,2))./Y(:,3)*1e3;
ECT_all=(Y(:,4)+Y(:,5))/2;
group_all=group; %bernal

mask_NC_Bernal=(group==1) ; %%PD

NHV=NHV_all(mask_NC_Bernal);
ECT=ECT_all(mask_NC_Bernal);
group=group_all(mask_NC_Bernal)+1; %%HC
time_BS_Bernal=X_Hipp(mask_NC_Bernal,2);

NHV_Check =(Tresults.LHippVol+Tresults.RHippVol)./Tresults.ICV*1e3;
ECT_Check =(Tresults.L_ECT+Tresults.R_ECT)/2;

gr_check=double(strcmp(Tresults.diagnose,'PD'));

figure(1);
subplot(1,2,1);lme_lowessPlot([Tresults.years;time_BS_Bernal],[NHV_Check;NHV],.95,[gr_check;group]);
title('Smoothed mean measurement trajectories');
ylabel('Normalized hippocampal volume');
xlabel('Time from baseline (in years)');
legend('NC_{our}','PD_{our}','NC_{Bernal}');

subplot(1,2,2);lme_lowessPlot([Tresults.years;time_BS_Bernal],[ECT_Check;ECT],.8,[gr_check;group]);
title('Smoothed mean measurement trajectories');
ylabel('ECT [mm]');
xlabel('Time from baseline (in years)');
legend('NC_{our}','PD_{our}','NC_{Bernal}');


%% Testing atrophy
time_BS_Bernal=X_Hipp(mask_NC_Bernal,2);
ni_Bernal=get_ni(time_BS_Bernal,[]);
BlAge=X_Hipp(mask_NC_Bernal,12);
atrophy_Bernal_NHV_NC=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    NHV_all(mask_NC_Bernal));
atrophy_Bernal_ECT_NC=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    ECT_all(mask_NC_Bernal));


% mask_NC_Check= gr_check==0; 
% time_BS_Check=Tresults.years(mask_NC_Check);
% ni_Check=get_ni(time_BS_Check,[]);
% BlAge=Tresults.Age(mask_NC_Check)-time_BS_Check;
% atrophy_Check_NHV_sNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
%     NHV_Check(mask_NC_Check));
% atrophy_Check_ECT_sNC=getAtrophy([time_BS_Check,BlAge],ni_Check,...
%     ECT_Check(mask_NC_Check));

mask_NC_Check= gr_check==1; 
time_BS_Check=Tresults.years(mask_NC_Check);
ni_Check=get_ni(time_BS_Check,[]);
BlAge=Tresults.Age(mask_NC_Check)-time_BS_Check;
atrophy_Check_NHV_PD=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    NHV_Check(mask_NC_Check));
atrophy_Check_ECT_PD=getAtrophy([time_BS_Check,BlAge],ni_Check,...
    ECT_Check(mask_NC_Check));


clc;
fprintf('NHV\n');
fprintf('Baseline Bernal NHV: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_NHV_NC{1}),std(atrophy_Bernal_NHV_NC{1}));

fprintf('Baseline Check  NHV: %.2f %.2f (NC) %.2f %.2f(PD)\n',...    %mean(atrophy_Check_NHV_sNC{1}),std(atrophy_Check_NHV_sNC{1}),...
    mean(atrophy_Check_NHV_PD{1}),std(atrophy_Check_NHV_PD{1}));

fprintf('Atrophy  Bernal NHV: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_NHV_NC{2}),std(atrophy_Bernal_NHV_NC{2}));

fprintf('Atrophy  Check  NHV: %.2f %.2f (NC) %.2f %.2f(PD)\n',...  %  mean(atrophy_Check_NHV_sNC{2}),std(atrophy_Check_NHV_sNC{2}),...
    mean(atrophy_Check_NHV_PD{2}),std(atrophy_Check_NHV_PD{2}));

fprintf('ECT\n');
fprintf('Baseline Bernal ECT: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_ECT_NC{1}),std(atrophy_Bernal_ECT_NC{1}));

fprintf('Baseline Check  ECT: %.2f %.2f (NC) %.2f %.2f(PD)\n',... %   mean(atrophy_Check_ECT_sNC{1}),std(atrophy_Check_ECT_sNC{1}),...
    mean(atrophy_Check_ECT_PD{1}),std(atrophy_Check_ECT_PD{1}));

fprintf('Atrophy  Bernal ECT: %.2f %.2f (NC)\n',...
    mean(atrophy_Bernal_ECT_NC{2}),std(atrophy_Bernal_ECT_NC{2}));

fprintf('Atrophy  Check  ECT: %.2f %.2f (sNC) %.2f %.2f(pNC)\n',... %   mean(atrophy_Check_ECT_sNC{2}),std(atrophy_Check_ECT_sNC{2}),...
    mean(atrophy_Check_ECT_PD{2}),std(atrophy_Check_ECT_PD{2}));


figure(2);
subplot(2,2,1);histogram(atrophy_Bernal_NHV_NC{1});hold on;  %%sNC es PD% MCI- ???
% histogram(atrophy_Check_NHV_sNC{1});hold off;
title('Baseline NHV sNC');
subplot(2,2,3);histogram(atrophy_Bernal_NHV_NC{2});hold on;
% histogram(atrophy_Check_NHV_sNC{2});hold off;
title('Atrophy NHV sNC');

subplot(2,2,2);histogram(atrophy_Bernal_NHV_NC{1});hold on;
histogram(atrophy_Check_NHV_PD{1});hold off;
title('Baseline NHV PD');

subplot(2,2,4);histogram(atrophy_Bernal_NHV_NC{2});hold on;
histogram(atrophy_Check_NHV_PD{2});hold off;
title('Atrophy NHV PD');

figure(3);

subplot(2,2,1);histogram(atrophy_Bernal_ECT_NC{1});hold on;
%histogram(atrophy_Check_ECT_sNC{1});hold off;
title('Baseline ECT sNC');

subplot(2,2,3);histogram(atrophy_Bernal_ECT_NC{2});hold on;
%histogram(atrophy_Check_ECT_sNC{2});hold off;
title('Atrophy ECT sNC');

subplot(2,2,2);histogram(atrophy_Bernal_ECT_NC{1});hold on;
histogram(atrophy_Check_ECT_PD{1});hold off;
title('Baseline ECT PD');

subplot(2,2,4);histogram(atrophy_Bernal_ECT_NC{2});hold on;
histogram(atrophy_Check_ECT_PD{2});hold off;
title('Atrophy ECT PD');




 end

function ni = get_ni(time_visit,numIDs)
baseline=find(time_visit==0);
numScans=length(time_visit);
ni=baseline([2:end,end])-baseline;
ni(end)=numScans-baseline(end)+1;
if(isempty(numIDs)==0)
    if(length(ni)~=numIDs)
        warning('Error calculating ni: There are more scans in baseline than subjects');
    end
end
if(sum(ni)~=numScans)
   warning('Error calculating ni');
end

end


function atrophy=getAtrophy(X,ni,HippMarker)


time=X(:,1);
BlAge=X(:,2);


BlAge_ni=BlAge(time==0);

atrophy=cell(2,1);
intercept=ones(length(time),1);
X_Hipp=[intercept,time];%,BlAge];
model=lme_fit_FS(X_Hipp,[1 2],HippMarker,ni);

% atrophy{1}=model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:);
% atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
%     ((model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:)))*100);

atrophy{1}=model.Bhat(1)+model.bihat(1,:);
atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
    ((model.Bhat(1)+model.bihat(1,:)))*100);


end


