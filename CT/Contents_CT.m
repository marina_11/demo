%% Flow-chart
% 1. mass_get_Thickness_mgh (freesurfer)
% 2. mass_joint_CTdata_PPMI
% 3. mass_buildMassLmeModels
% 4. mass_saveThicknessPvalueMap 
% 5. mass_generate_Clusters (freesurfer) (TH: thmin 2 sMCI vs pMCI)
% 6. mass_cluster2table

% Test
% 1. ST-LME power (NC vs AD)
% 2. controlTypeI error (NC vs NC)
%% Function
%mass_get_Thickness_mgh: read cortical thickness from Freesurfer longitudinal framework
%mass_joint_CTdata_PPMI: join *.mgh from several processing batches
%mass_buildMassLmeModels: build ST-LME Models from *.mgh and clinical data
%mass_saveThicknessPvalueMap: save q-value into *.mgh (see getFDRMaps).
%mass_cluster2table: read CT from cluster to table


%Test
%mass_sensitivitySTLME: calculate ST-LME power (see Bernal13b fig.1)
%mass_controlTypeI: error type I ST-LME (see Bernal13b table 2)
