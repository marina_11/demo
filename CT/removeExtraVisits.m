function [Tdec, Y_lh, Y_rh] = removeExtraVisits(Tdec, Y_lh, Y_rh)
    mask_extra_visits=ones(size(Tdec.fsidbase,1),1);
    fsid=string(Tdec.fsid);
    for i=1:(size(fsid,1)-1)
        if (strcmp(fsid(i),fsid(i+1)))
            mask_extra_visits(i+1)=0;
        end
    end
    mask_extra_visits=logical(mask_extra_visits);
    Tdec=Tdec(mask_extra_visits,:);
    Y_lh=Y_lh(mask_extra_visits,:);
    Y_rh=Y_rh(mask_extra_visits,:);
end