function mass_get_Thickness_mgh
%function tableID=mass_get_Thickness_mgh

%% Description
% In this case the interest focus on determining regionally specific 
% differences in cortical thickness atrophy rate over time among the two 
% groups of individuals: sMCI and pMCI. The following 
% Freesurfer's commands were used to generate the spatial cortical
% thickness data:
% 
% mris_preproc
% mri_surf2surf

%% Initialization of FreeSurfer in bash terminal
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% export SUBJECTS_DIR= Long directory


%% Defining paths and files
orig_pwd = pwd;
SUBJECTS_DIR='/home/mruiz/Desktop/alvaro/Long/';
cd(SUBJECTS_DIR);

% File with clinical information (qdec file)
%name_dat='clinicaldata_long_conversores_precl_V160.dat';
%name_dat='clinicaldata_long_conversores_precl_V126.dat';
% name_dat='clinicaldata_long_conversores_precl_V60.dat';
name_dat='clinicaldata_long_V173.dat';

qdecfile = sprintf('%s',name_dat);
qdecOption = 'qdec-long';
%qdecOption = 'qdec';

% output file base name
%name_ct='V160_long_mass_CT';
%name_ct='V126_long_mass_CT';
% name_ct='V60_long_mass_CT';
name_ct='V68_long_mass_CT';

% If desired, surfaces can also be referenced to the fsaverage6 subject
% instead of fsaverage. However, it would be necessary to change it in
% every other step.

simboLink = sprintf('ln -s $FREESURFER_HOME/subjects/fsaverage');
fprintf(strcat(simboLink,'\n'));
system(simboLink);

% mris_preproc is charged of reassembling data into a common space. In this
% case, to the fsaverage subject (around 160.000 vertex). It also
% concatenates the different subjects/visits into a single file
% (?h.thickness.mgh). It is repeated for both hemishperes.
%
% --qdec-long refers to the type of processing
% --target refers to the subject which the data will be reassembled into
% --hemi refers to the hemisphere
% --meas refers to the measure studied
% --out refers to the output file.


preproc = sprintf('mris_preproc --%s %s --target fsaverage --hemi lh --meas thickness --out lh.thickness.mgh',...
    qdecOption,qdecfile);
fprintf(strcat(preproc,'\n'));
system(preproc);

preproc = sprintf('mris_preproc --%s %s --target fsaverage --hemi rh --meas thickness --out rh.thickness.mgh',...
    qdecOption,qdecfile);
fprintf(strcat(preproc,'\n'));
system(preproc);





% mri_surf2surf resamples the vertex and thickness by smoothing it into the
% fsaverage surface again.
% --hemi refers to the hemisphere
% --s refers to the subject taken as base for the vertex position
% --sval refers to the file where values are stored
% --tval refers to the file where results will be stored. Same size as sval
% --fwhm-trg smooths surface to full width half maximum of (eg) 10
% --cortex only smooths vertices that are within the cortex label
% --noreshape fixes points as they were already reassembled in the last
%             step (mris_preproc).



surf2surf = sprintf('mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.%s_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape',...
    name_ct); 
fprintf(strcat(surf2surf,'\n'));
system(surf2surf);

surf2surf = sprintf('mri_surf2surf --hemi rh --s fsaverage --sval rh.thickness.mgh --tval rh.%s_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape',...
    name_ct); 
fprintf(strcat(surf2surf,'\n'));
system(surf2surf);


% Removal of the symbolic link to fsaverage.
system('unlink fsaverage');
cd(orig_pwd);


end
