function [Tdec,Y_lh,Y_rh]=readCT_mass_info(path_MGH,fclinical_data)

% addpath('./external/lme/');
% addpath('./external/lme/Qdec/');

%% Read demographic and clinical data
% fclinical_data='./data/V160/clinicaldata_long_conversores_precl_V160.dat'; 
% path_MGH='./data/V160/';

Tdec=readClinicalData(fclinical_data);

file_CT=dir(strcat(path_MGH,'*_sm10_fs.mgh'));
[Y_lh,~] = fs_read_Y(strcat(path_MGH,file_CT(1).name));
[Y_rh,~] = fs_read_Y(strcat(path_MGH,file_CT(2).name));


% rmpath('./external/lme/');
% rmpath('./external/lme/Qdec/');

end






function Tdec=readClinicalData(fname)

Qdec = fReadQdec(fname);
Tdec=cell2table(Qdec(2:end,:));
varname=cellstr(Qdec(1,:));
varname{2}='fsidbase';
Tdec.Properties.VariableNames=varname;
Tdec.years=str2double(Tdec.years);
Tdec.Age=str2double(Tdec.Age);
Tdec.GDS=str2double(Tdec.GDS);
Tdec.fsidbase=str2double(Tdec.fsidbase);
Tdec.ID=str2double(Tdec.ID);
Tdec.imageUID=str2double(Tdec.imageUID);


end

