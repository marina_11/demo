function ni = get_ni(time_visit)
    % Function charged of calculating the number of images for each
    % subject.
    mask_baseline=time_visit==0;
    numScans=length(time_visit);
    ni=zeros(sum(mask_baseline),1);
    j=1;
    for i=1:length(ni)
        if(mask_baseline(j)==1 && j<numScans )
            j=j+1;
            k=1;
            while(time_visit(j)>0 && j<numScans)
                j=j+1;
                k=k+1;
            end
            ni(i)=k;
            if(j==numScans)
                ni(i)=k+1;
            end
        elseif(j==numScans)
            ni(i)=1;
        else
            warning('Error calculating ni');
        end
        
    end
    
end