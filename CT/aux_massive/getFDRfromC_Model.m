function [F_LH,F_RH]=getFDRfromC_Model(nameMATmodel)

load(nameMATmodel,'model_LH','model_RH');
F_LH =getFstatLMEmassModel(model_LH);
F_RH =getFstatLMEmassModel(model_RH);

end

function F_stats =getFstatLMEmassModel(LmeMassModel)
% Structure array containing a contrast matrix CM.C for each and every
% voxel/vertex
%X=[ones(size(Tresults,1),1),time_bsl,gr_PD,time_BS.*gr_PD,BlAge];    
CM.C = [0 0 1 0 0];
% Output:
% fstats.F: F-Statistic.
% fstats.pval: P-value of the F-Statistic.
% fstats.sgn: Sign of the contrast.
% fstats.df: Degrees of freedom of the F-Statistic.
F_stats = lme_mass_F(LmeMassModel,CM);
end