function [mask_scan_train,ni_two_train,ni_two_test] = ...
        splitSamples(ni_two,percent)
    %This function is charged of, given the number of images (ni_two) and a
    %percentage of training data desired, to give back a mask of the
    %training subset of subjects and the number of images.
    
    %% Initialization and random test selection
    numPatients=length(ni_two);
    p = randperm(numPatients);
    test=p(fix(percent*numPatients)+1:end);
    flag_train=true(numPatients,1);    
    for i=1:length(test)
        flag_train(test(i))=false;
    end
    
    %% Expansion of the test selection to all images of those subjects
    % Two counters (j and k) are used in order to navigate through the
    % vectors of number of images (ni_two_Train and ni_two_test).
    ni_two_train=zeros(sum(flag_train==true),1);
    ni_two_test=zeros(numPatients-sum(flag_train==true),1);
    j=1;
    k=1;
    numImg=1;
    mask_scan_train=true(sum(ni_two),1);
    for i=1:numPatients
        if(flag_train(i)==true)
            ni_two_train(j)= ni_two(i);
            j=j+1;
        else
            ni_two_test(k)= ni_two(i);
            k=k+1;
            mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
        end
        numImg=numImg+ni_two(i);
    end
    
end