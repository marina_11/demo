function [Tdec,Y_lh,Y_rh]=readCT_mass_info_ADNI(path_MGH,fclinical_data)

% addpath('./external/lme/');
% addpath('./external/lme/Qdec/');

%% Read demographic and clinical data
% fclinical_data='./data/V160/clinicaldata_long_conversores_precl_V160.dat'; 
% path_MGH='./data/V160/';

Tdec=readClinicalData(fclinical_data);

file_CT=dir(strcat(path_MGH,'*_sm10_fs.mgh'));
[Y_lh,~] = fs_read_Y(strcat(path_MGH,file_CT(1).name));
[Y_rh,~] = fs_read_Y(strcat(path_MGH,file_CT(2).name));
Tdec.APOE_A1=[];
Tdec.APOE_A2=[];
Tdec.CDR=[];
% Months=Tdec.years*12;
% Tdec.diagnose=num2cell(zeros(size(Tdec.fsidbase,1),1));
% Tdec.visit=num2cell(zeros(size(Tdec.fsidbase,1),1));
% for i=1:size(Tdec,1)
%     if(Months(i)<2)
%         Tdec.visit(i)=cellstr('Baseline');
%     elseif Months(i)>=2 && Months(i)<=9
%         Tdec.visit(i)=cellstr('Month_6');
%      elseif Months(i)>9 && Months(i)<=18
%          Tdec.visit(i)=cellstr('Month_12');
%      elseif Months(i)>18 && Months(i)<=30
%          Tdec.visit(i)=cellstr('Month_24');
%      elseif Months(i)>40 && Months(i)<=50
%          Tdec.visit(i)=cellstr('Month_48');
%     else
%         Tdec.visit(i)=cellstr('Other');
%     end
%     Tdec.diagnose(i)=cellstr('Control');
% end
% Tdec=Tdec(:, [1:3 5 4 6 7 11 10 8 9]);
% Visit=grp2idx(Tdec.visit);
% mask_visits= (Visit==1) | (Visit==3) | (Visit==4) | (Visit==5);
% Tdec=Tdec(mask_visits,:);
% Y_lh=Y_lh(mask_visits,:);
% Y_rh=Y_rh(mask_visits,:);
% rmpath('./external/lme/');
% rmpath('./external/lme/Qdec/');

end






function Tdec=readClinicalData(fname)

Qdec = fReadQdec(fname);
Tdec=cell2table(Qdec(2:end,:));
varname=cellstr(Qdec(1,:));
varname{2}='fsidbase';
Tdec.Properties.VariableNames=varname;
Tdec.years=str2double(Tdec.years);
Tdec.Age=str2double(Tdec.Age);
Tdec.GDS=str2double(Tdec.GDS);
fsidbase=char(Tdec.fsidbase);
Tdec.fsidbase=string(Tdec.fsidbase);
for i=1:size(Tdec,1)
    Tdec.fsidbase(i)=strcat(fsidbase(i,1:3),fsidbase(i,7:10));
end
Tdec.fsidbase=str2double(Tdec.fsidbase);
Tdec.ID=Tdec.fsidbase;
Tdec.imageUID=zeros(size(Tdec.fsidbase,1),1);
Tdec.MMSE=[];


end

