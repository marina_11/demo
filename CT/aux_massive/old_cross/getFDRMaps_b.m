function [qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps_b(Y_lh,Y_rh,group)

%addpath('./aux_massive/');

% [Y_lh,Y_rh,table_family,~]= joint_massCT_T1_clinical;
% group=grp2idx(table_family.Etiqueta);
labels=unique(group);

n_boot=16;
numSubjects=size(Y_lh,1);
numVoxels=size(Y_lh,2);
p_value_lh=zeros(numVoxels,n_boot);
p_value_rh=zeros(numVoxels,n_boot);
sign_lh=zeros(numVoxels,n_boot);
sign_rh=zeros(numVoxels,n_boot);

parfor i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    mask_scan_train=splitSamples_cross(numSubjects,.75);
    
    for j=1:numVoxels
        [p_value_lh(j,i),~,stat]=ranksum(Y_lh(mask_scan_train & group==labels(1),j),...
            Y_lh(mask_scan_train & group==labels(2),j),'method','approximate');
        
        if(isnan(p_value_lh(j,i)))
            p_value_lh(j,i)=1;
        end
        sign_lh(j,i)=stat.zval;
        
        p_value_rh(j,i)=ranksum(Y_rh(mask_scan_train & group==labels(1),j),...
            Y_rh(mask_scan_train & group==labels(2),j),'method','approximate');
        if(isnan(p_value_rh(j,i)))
            p_value_rh(j,i)=1;
        end
        sign_rh(j,i)=stat.zval;
        
    end

end

sign_lh(sign_lh>0)=1;
sign_rh(sign_rh>0)=1;
sign_lh(sign_lh<0)=-1;
sign_rh(sign_rh<0)=-1;


% penultimate parameter: Expected FDR. Can be modified.
% last parameter: tail (-1=left-sided, 0=two-sided, 1=right-sided)
%                 hypothesis testing.

% With all the data of the x models the q-values are calculated for both
% hemispheres.
% Output
% detvtx: Detected vertices (1-based).
% spval: Unsigned sided p-values.
% pth: FDR threshold.
% m0: Estimated number of null vertices.

% Significance level desired
expected_FDR = 0.05;


[~,qval_lh,~,~] = lme_mass_FDR2(p_value_lh,sign_lh,[],expected_FDR,0);
qval_lh = qval_lh';
sign_lh = sum(sign_lh,2)/n_boot;

[~,qval_rh,~,~] = lme_mass_FDR2(p_value_rh,sign_rh,[],expected_FDR,0);
qval_rh = qval_rh';
sign_rh = sum(sign_rh,2)/n_boot;


%rmpath('../../lme/mass_univariate/');
%rmpath('./aux_massive/');

end