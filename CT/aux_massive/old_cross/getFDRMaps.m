function [qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps
% This function is charged of getting the FDR maps from the massive
% information. Depending on the groups specified the input will be taken
% from one folder or another. Thanks to lme_mass_FDR2 the FDR will be built
% and returned in form of the q-values, which are similar to the p-values
% but having evaluated several tests (subsets).

addpath('../../lme/mass_univariate/');


%% Read ST-LME models
path_MAT='./data/';
listMAT=dir(strcat(path_MAT,'mass_family_*.mat'));


%% Initialization

% The extraction will occur for each subject.
n_boot=numel(listMAT);

% fsaverage number of Voxels.
numVoxels=163842; 

% Significance level desired
expected_FDR = 0.05;


% The p-values and significance for each vertex are stored into a matrix, 
% where each row is a vertex, and each column is a model.

pvalue_lh = zeros(numVoxels,n_boot);
pvalue_rh = zeros(numVoxels,n_boot);
sgn_lh = zeros(numVoxels,n_boot);
sgn_rh = zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    % Information about left hemisphere and right hemisphere F-statistics
    % is loaded.
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    
   
    %% Mass data extraction from the model
    pvalue_lh(:,i) = F_LH.pval;
    sgn_lh(:,i) = F_LH.sgn;
    
    pvalue_rh(:,i) = F_RH.pval;
    sgn_rh(:,i) = F_RH.sgn;
 
end


% penultimate parameter: Expected FDR. Can be modified.
% last parameter: tail (-1=left-sided, 0=two-sided, 1=right-sided)
%                 hypothesis testing.

% With all the data of the x models the q-values are calculated for both
% hemispheres.
% Output
% detvtx: Detected vertices (1-based).
% spval: Unsigned sided p-values.
% pth: FDR threshold.
% m0: Estimated number of null vertices.

[~,qval_lh,pth_lh,m0_lh] = lme_mass_FDR2(pvalue_lh,sgn_lh,[],expected_FDR,0);
qval_lh = qval_lh';
sign_lh = sum(sgn_lh,2)/n_boot;

[~,qval_rh,pth_rh,m0_rh] = lme_mass_FDR2(pvalue_rh,sgn_rh,[],expected_FDR,0);
qval_rh = qval_rh';
sign_rh = sum(sgn_rh,2)/n_boot;


rmpath('../../lme/mass_univariate/');


end

