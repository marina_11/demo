function [Y_lh,Y_rh,out,delete_scans]= joint_massCT_T1_clinical

% addpath('../../lme/');

load('./data/ID_mass_CT_family','ID_mass_CT_family');
load('./data/Family200219','out');

T1=string(ID_mass_CT_family.name);
T1=extractAfter(T1,7);
lim=strfind(T1,'_');
for i=1:numel(T1)
    T1(i)=extractBefore(T1(i),lim{i}(1));
end
T1(end)="FAMCTB026";

index=zeros(numel(T1),1);
delete_scans=[];
for i=1:numel(T1)
    tmp=find(strcmp(out.CodigoID,T1(i)));
    if(length(tmp)==1)
        index(i)=tmp;
    elseif(length(tmp)>1)
        fprintf('Warning: scans %s with more than one ID in table\n',Ti(i));
    else
        delete_scans=[delete_scans;i];
    end
        
end

index_inv=zeros(size(out,1),1);
for i=1:size(out,1)
    index_inv(i)=find(strcmp(T1,out.CodigoID(i)));      
end

path_MGH='./data/';
file_CT=dir(strcat(path_MGH,'*_sm10_fs.mgh'));
% file_CT=dir(strcat(path_MGH,'*thickness.mgh'));
[Y_lh,~] = fs_read_Y(strcat(path_MGH,file_CT(1).name));
[Y_rh,~] = fs_read_Y(strcat(path_MGH,file_CT(2).name));

Y_lh=Y_lh(index_inv,:);
Y_rh=Y_rh(index_inv,:);

% rmpath('../../lme/');

end