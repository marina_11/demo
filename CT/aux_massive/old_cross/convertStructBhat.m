function Bhat=convertStructBhat(lhstats)
    %Converts a model struct (lhstats) into a Bhat type 
    Bhat=zeros(size(lhstats(1).Bhat,1),size(lhstats,2));
    for i=1:size(lhstats,2)
        if(isempty(lhstats(i).Bhat)==0)
            Bhat(:,i)=lhstats(i).Bhat;
        end
    end
end