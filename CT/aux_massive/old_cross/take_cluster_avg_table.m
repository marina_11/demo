function clusterTable = ...
        take_cluster_avg_table (populationfile,clusterfile, hemi)
    % This results in a NxnVtx table where N is the number of subjects and nVtx
    % is the number of vertex. Usually this will be around 160.000 as the file
    % was created resampling data to fsaverage subject mesh.
    % population file is the mgh file with all population thickness for each
    % vertex
    
    % cluster file is the mgh file that groups all labels
    
    % hemi is the hemisphere option.
    
    population_thickness = fs_read_Y(populationfile);
    
    % This will give a table of 4xnVtx. The 4th row contains the label for all
    % the clusters obtained in surfCluster.
    clusterinfo = fs_read_Y(clusterfile);
    
    % Originally Clusterindex will have a 0 label that corresponds to all
    % vertex that were not included in any original label.
    clusterindex = unique(clusterinfo(4,:));
    clusterindex(find(clusterindex==0))=[];
    
    cluster_names = cell(1,numel(clusterindex));
    cluster_values = zeros(size(population_thickness,1),numel(clusterindex));
    
    for i=1:numel(clusterindex)
        cluster_names{i} = strcat(hemi,'_cl',num2str(i));
        vertex_selection = find(clusterinfo(4,:) ==i);
        tmp_selection = population_thickness(:,vertex_selection);
        cluster_values(:,i)=mean(tmp_selection,2);
    end
    
    clusterTable= array2table(cluster_values, 'VariableNames', cluster_names);
    
end