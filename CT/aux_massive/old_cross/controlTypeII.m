function power=controlTypeII(pvalue,sgn,alpha_th)
    power=zeros(length(alpha_th),1);
    numVertex=size(pvalue,1);
    for i=1:length(alpha_th)
        [~,~,~,m0] = lme_mass_FDR2(pvalue,sgn,[],alpha_th(i),0);
        power(i)=(numVertex-m0)/numVertex;
    end
    
end