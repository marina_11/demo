function [lhRgs,lhstats,F_lhstats] = getMassLme(X,Y,ni,hcortex,hsphere)
    %% Initial vertex-wise temporal covariance estimates
    % Starting values at each location for the linear mixed-effects iterative
    % estimation. This function should give better estimators than the
    % lme_mass_fit_init function but at the cost of extra computational time.
    %
    %lme_mass_fit_EMinit(X,Zcols,Y,ni,mask vertices,maximization iterations);
    % Output: Theta0 and residual errors matrix at each location.
    
    [lhTh0,lhRe] = lme_mass_fit_EMinit(X,1,Y,ni,hcortex,3);
    
    
    %% Spherical proyection
    % lme_mass_RgGrow implements a region growing algorithm along the spherical
    % surface to find homogeneous regions comprising locations with similar
    % covariance components.
    % Output
    % Regions: 1 x nv segmentation vector containing a region number assigned
    % to each vertice along the surface.
    [lhRgs,lhRgMeans] = lme_mass_RgGrow(hsphere,lhRe,lhTh0,hcortex,2,95);
    
    
    %% Display
    
    % Here both lhTh0 and lhRgMeans can be overlaid onto lhsphere and visually
    % compared each other to ensure that they are similar enough
    % (the essential spatial organization of the initial covariance estimates
    % was not lost after the segmentation procedure, otherwise the above input
    % value 2 must be reduced to 1.8 and so on):
    
    % surf.faces =  lhsphere.tri;
    % surf.vertices =  lhsphere.coord';
    %
    % figure; p1 = patch(surf);
    % set(p1,'facecolor','interp','edgecolor','none','facevertexcdata',lhTh0(1,:)');
    %
    % figure; p2 = patch(surf);
    % set(p2,'facecolor','interp','edgecolor','none','facevertexcdata',lhRgMeans(1,:)');
    
    
    %% Fitting the model
    % The spatiotemporal LME model was fitted using:
    lhstats = lme_mass_fit_Rgw(X,1,Y,ni,lhTh0,lhRgs,hsphere);
    
    % Structure array containing a contrast matrix CM.C for each and every
    % voxel/vertex
    % X=[ones,time_scan,group,group*time,age_baseline]
    % X=[ones,group,age_baseline]    
    %CM.C = [0 0 1 0 0];
    CM.C = [0 1 0];
    
    % Output:
    % fstats.F: F-Statistic.
    % fstats.pval: P-value of the F-Statistic.
    % fstats.sgn: Sign of the contrast.
    % fstats.df: Degrees of freedom of the F-Statistic.
    
    F_lhstats = lme_mass_F(lhstats,CM);
    
end