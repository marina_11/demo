function [feat,beta]=linearCovCorrection_cross(feat,covariates)

beta=zeros(size(covariates,2),size(feat,2));
for i=1:size(feat,2)
    [feat(:,i),beta(:,i)]=linearRegresionCov(feat(:,i),covariates);
end

end

function [marker_corr,beta]=linearRegresionCov(marker,covariates)
X=[ones(size(covariates,1),1),covariates];
beta=X\marker;
marker_corr=marker-covariates*beta(2:end);
beta=beta(2:end);
end