function feat=linearCovCorrectionBeta_cross(feat,covariates,beta)

for i=1:size(feat,2)
    feat(:,i)=linearRegresionCovBeta(feat(:,i),covariates,beta(:,i));
end

end




function marker_corr=linearRegresionCovBeta(marker,covariables,beta)
marker_corr=marker-covariables*beta;
end