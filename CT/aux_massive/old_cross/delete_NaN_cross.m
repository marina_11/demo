function [feat,delete_scans] = delete_NaN_cross(feat,thresh)

matrix_nan=isnan(feat);
num_nan_scan=sum(matrix_nan');
[frec,scans]=sort(num_nan_scan,'descend');
delete_scans=scans(frec>thresh);
feat(delete_scans,:)=[];
feat = clean_NaN_cross(feat);

end