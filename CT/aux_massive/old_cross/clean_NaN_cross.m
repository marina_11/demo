function feat = clean_NaN_cross(feat)

mean_sin_nan=mean(feat,'omitnan');
matrix_nan=isnan(feat);
for i=1:size(matrix_nan,2)
   feat(matrix_nan(:,i),i)=mean_sin_nan(i);
end
end