function [mask_scan_train,mask_scan_test]=splitSamples_cross(numPatients,percent)
%
%
% Author: Carlos Platero
% Date: 2018/10/07
% 
% General inquiries: carlos.platero@upm.es
%


p = randperm(numPatients);
test=p(fix(percent*numPatients)+1:end);
mask_scan_train=true(numPatients,1);

for i=1:length(test)
    mask_scan_train(test(i))=false;
end

mask_scan_test=(mask_scan_train==false);
end