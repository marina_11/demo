function [qval_lh,qval_rh,sign_lh,sign_rh]=mass_comparative_CT_family
% addpath('./aux_massive/');
% addpath('../NCtoMCI/external/lme/');
% addpath('../NCtoMCI/external/lme/mass_univariate/');

%% family data
[Y_lh,Y_rh,table_family,~]= joint_massCT_T1_clinical;
group=grp2idx(table_family.Etiqueta);
age=table_family.Edad;
mask_age = isnan(age)==0;
Y_lh=Y_lh(mask_age,:);
Y_rh=Y_rh(mask_age,:);
age=age(mask_age);
group=group(mask_age);

covariates=age;

% ADNI
% load('./data/beta_CT_ADNI_NC_Age','mean_Age_NC_bls','beta_NC_ADNI_lh','beta_NC_ADNI_rh');
% covariates=covariates-repmat(mean_Age_NC_bls,size(covariates,1),1);
% Y_lh=linearCovCorrectionBeta_cross(Y_lh,covariates,beta_NC_ADNI_lh);
% Y_rh=linearCovCorrectionBeta_cross(Y_rh,covariates,beta_NC_ADNI_rh);

% CorrectionAge with NC Age
mask_NC=group==1;%NC
meanCovariates=mean(covariates(mask_NC,:));
covariates=covariates-repmat(meanCovariates,size(covariates,1),1);
[~,beta_NC_lh]=linearCovCorrection_cross(Y_lh(mask_NC,:),covariates(mask_NC,:));
[~,beta_NC_rh]=linearCovCorrection_cross(Y_rh(mask_NC,:),covariates(mask_NC,:));

Y_lh=linearCovCorrectionBeta_cross(Y_lh,covariates,beta_NC_lh);
Y_rh=linearCovCorrectionBeta_cross(Y_rh,covariates,beta_NC_rh);


% v_l=abs(Y_lh_new-Y_lh)./Y_lh*100;
% v_r=abs(Y_rh_new-Y_rh)./Y_rh*100;

[qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps_b(Y_lh,Y_rh,group);

% rmpath('./aux_massive/');
% rmpath('../NCtoMCI/external/lme/');
% rmpath('../NCtoMCI/external/lme/mass_univariate/');

end

