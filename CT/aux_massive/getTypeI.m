function err_I = getTypeI(pval,alpha_th)
    % returns a value between 0 and 1 for each significance value tried
    % (alpha).
    numErr=length(alpha_th);
    samples=length(pval);
    err_I=zeros(numErr,1);
    for i=1:numErr
        err_I(i)=sum(pval<alpha_th(i))/samples;
    end
    
end