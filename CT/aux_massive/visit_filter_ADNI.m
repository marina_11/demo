function [Tdec,Y_lh,Y_rh]=visit_filter_ADNI(Tdec,Y_lh,Y_rh)
Months=Tdec.years*12;
Tdec.diagnose=num2cell(zeros(size(Tdec.fsidbase,1),1));
Tdec.visit=num2cell(zeros(size(Tdec.fsidbase,1),1));
for i=1:size(Tdec,1)
    if(Months(i)<2)
        Tdec.visit(i)=cellstr('Baseline');
    elseif Months(i)>=2 && Months(i)<=9
        Tdec.visit(i)=cellstr('Month_6');
     elseif Months(i)>9 && Months(i)<=18
         Tdec.visit(i)=cellstr('Month_12');
     elseif Months(i)>18 && Months(i)<=30
         Tdec.visit(i)=cellstr('Month_24');
     elseif Months(i)>40 && Months(i)<=50
         Tdec.visit(i)=cellstr('Month_48');
    else
        Tdec.visit(i)=cellstr('Other');
    end
    Tdec.diagnose(i)=cellstr('Control');
end
Tdec=Tdec(:, [1:3 5 4 6 7 11 10 8 9]);
Visit=grp2idx(Tdec.visit);
mask_visits= (Visit==1) | (Visit==3) | (Visit==4) | (Visit==5);
% Tdec=Tdec(mask_visits,:);
% Y_lh=Y_lh(mask_visits,:);
% Y_rh=Y_rh(mask_visits,:);

end