function [Tdec, Y_lh, Y_rh] = order_CT(Tdec, Y_lh, Y_rh)

% [Tdec, order]=sortrows(Tdec, 'years', 'ascend');
% 
% Y_lh=Y_lh(order,:);
% Y_rh=Y_rh(order,:);


[Tdec, order]=sortrows(Tdec,'fsid','ascend');

Y_lh=Y_lh(order,:);
Y_rh=Y_rh(order,:);

% [Tdec]=sortrows(Tdec, 'fsid', 'ascend');
% [Tdec, order]=sortrows(Tdec,'fsid','ascend');
% 
% Y_lh=Y_lh(order,:);
% Y_rh=Y_rh(order,:);


end