function [Tdec,Y_lh,Y_rh]=mass_joint_CTdata_PPMI

addpath('./external/lme/');
addpath('./external/lme/Qdec/');
addpath('./aux_massive');

%% Read demographic and clinical data
%V68
[Tdec_68,Y_lh_68,Y_rh_68]=readCT_mass_info('./data/V68(yo)/',...
    './data/V68(yo)/clinicaldata_long_68V_23s.dat');

%V173
[Tdec_173,Y_lh_173,Y_rh_173]=readCT_mass_info('./data/V173/',...
    './data/V173/clinicaldata_long_V173.dat');

%V19
[Tdec_19,Y_lh_19,Y_rh_19]=readCT_mass_info('./data/V19(yo)/',...
    './data/V19(yo)/clinicaldata_long_conversores_precl_V19.dat');

%V119
[Tdec_119,Y_lh_119,Y_rh_119]=readCT_mass_info('./data/V126(yo)/',...
    './data/V126(yo)/clinicaldata_long_conversores_precl_V126.dat');

%V60
[Tdec_60,Y_lh_60,Y_rh_60]=readCT_mass_info('./data/V60(yo)/',...
    './data/V60(yo)/clinicaldata_long_conversores_precl_V60.dat');

%V160
[Tdec_160,Y_lh_160,Y_rh_160]=readCT_mass_info('./data/V160(yo)/',...
    './data/V160(yo)/clinicaldata_long_conversores_precl_V160.dat');

%% all
Tdec=[Tdec_68;Tdec_173;Tdec_19;Tdec_119;Tdec_60;Tdec_160];
Y_lh=[Y_lh_68;Y_lh_173;Y_lh_19;Y_lh_119;Y_lh_60;Y_lh_160];
Y_rh=[Y_rh_68;Y_rh_173;Y_rh_19;Y_rh_119;Y_rh_60;Y_rh_160];
[Tdec, Y_lh, Y_rh] =order_CT(Tdec, Y_lh, Y_rh);


%% remove extra visits PPMI
[Tdec, Y_lh, Y_rh] = removeExtraVisits(Tdec, Y_lh, Y_rh);

%% Checking
Tresults=load('./data/Tresults_1113v_198s');
Tresults=Tresults.Tresults_1113v_198s;
A=~ismissing(Tresults.fsidbase);% mascara de missing
Tresults=Tresults(A,:);
Tdec.years=Tresults.years; %To ajust the years of the subjects without baseline

diffID=sum(abs(Tdec.fsidbase-Tresults.fsidbase));
if(diffID>0)
    warning('Visits are differents');
end
for i=1:size(Tresults,1)
    if(strcmp(Tresults.fsid(i),Tdec.fsid(i))==0)
        warning('Visits are differents');
    end
end

end




